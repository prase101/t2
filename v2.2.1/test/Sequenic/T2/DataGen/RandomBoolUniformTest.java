package Sequenic.T2.DataGen;

import org.junit.Test;
import static org.junit.Assert.*;

public class RandomBoolUniformTest {

    @Test
    public void test1() {
        RandomBoolUniform R = new RandomBoolUniform(0.5) ;
        int tt = 0 ;
        int ff = 0 ;
        for (int i=0; i<30; i++) {
            if (R.nextValue()) tt++ ; else ff++ ;
        }
        assertTrue(tt>0) ;
        assertTrue(ff>0) ;
    }
    
}