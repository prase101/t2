package Sequenic.T2;

import Sequenic.T2.*;
import java.io.*;
import java.util.*;
import org.junit.Test;

public class Main_Test_4 {

    /**
     * Test purpose: check if T2 pass custom pool correctly.
     */
    static public class Cpool {

        void m0(Cpool x) {
            assert false;
        }

        void m1(Cpool x) {
        }
    }

    static public class CustomPool0 extends Pool {
    }

    @Test
    public void test_pool() {
        System.out.println("@@@ check if T2 pass custom pool correctly...");
        Class C = Cpool.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --pool=" + CustomPool0.class.getName() + " --debug --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --pool=" + CustomPool0.class.getName() + " --debug --silent");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: check if T2 pass custom interface-map correctly.
     */
    static public class Cimap {

        void m0(Cimap x) {
            assert false;
        }

        void m1(Cimap x) {
        }
    }

    static public class CustomInterfaceMap0 extends InterfaceMap0 {
    }

    @Test
    public void test_imap() {
        System.out.println("@@@ check if T2 pass custom interface-map correctly...");
        Class C = Cimap.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --imap=" + CustomInterfaceMap0.class.getName() + " --debug --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --imap=" + CustomInterfaceMap0.class.getName() + " --debug --silent");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: check if T2 pass custom base domain correctly.
     */
    static public class Cbdomain {

        void m0(Cbdomain x) {
            assert false;
        }

        void m1(Cbdomain x) {
        }
    }

    static public class CustomBDomain0 extends BaseDomainSmall {
    }

    @Test
    public void test_bdomain() {
        System.out.println("@@@ check if T2 pass custom base domain correctly...");
        Class C = Cbdomain.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --bdomain=" + CustomBDomain0.class.getName() + " --debug --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --bdomain=" + CustomBDomain0.class.getName() + " --debug --silent");
        Debug.assertTrfEmpty(C);
    }
}
