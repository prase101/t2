package Sequenic.T2.Predicate;

import org.junit.Test;
import static org.junit.Assert.* ;
import java.util.*;
import static Sequenic.T2.Predicate.Operators.* ;

public class OperatorsTest {

	static class P extends Predicate {
		
		public Boolean check(Object x) {
			return (Integer) x > 9 ;
		}
		
	}
	
	@Test
	public void	forall_test() {
		Collection dom = new LinkedList<Integer>() ;
		assertTrue(forall(dom,new P())) ;
		dom.add(1) ;
		assertTrue(! forall(dom,new P())) ;
		dom.clear() ;
		dom.add(10) ;
		dom.add(99) ;
		assertTrue(forall(dom,new P())) ;	
	}
	
	@Test
	public void	exists_test() {
		Collection dom = new LinkedList<Integer>() ;
		assertTrue(!exists(dom,new P())) ;
		dom.add(1) ;
		assertTrue(! exists(dom,new P())) ;
		dom.add(10) ;
		assertTrue(exists(dom,new P())) ;	
	}
	
	static class Cell {
		int item = 0 ;
	}
	
	// Automaton encoded as predicate:
	static class M extends Predicate {
		
		int state = 0 ;
		
		public M() { }
		
		public Boolean check(Object x) {
			Cell c = (Cell) x ;
			if (state==0 && c.item>9) {
				state=1 ;
				return true ;
			}
			if (state==1 && c.item<0) {
				state=2 ;
				return false ;
			}
			if (state==2) return false ;
			return true ;
		}
	}
	
	@Test
	public void forallMap_test() {
		Collection<Cell> dom = new LinkedList<Cell>();
		Map<Cell,M> mapping = new HashMap<Cell,M>() ;
		assertTrue(forall(dom,M.class,mapping)) ;
		
		dom.add(new Cell()) ;
		Cell x = new Cell() ;
		dom.add(x) ;
		assertTrue(forall(dom,M.class,mapping)) ;
		
		x.item = 10 ;
		assertTrue(forall(dom,M.class,mapping)) ;
		x.item = -1 ;
		assertTrue(! forall(dom,M.class,mapping)) ;
		assertTrue(! forall(dom,M.class,mapping)) ;
	}
	
}
