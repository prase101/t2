package Sequenic.T2;

import Sequenic.T2.*;
import org.junit.Test;

public class Main_Test_7 {
    
    static public class C {
        
        int x ;
        
        public C(int y) { x = y ; }
        
        public void m() { x++ ; x-- ; }
        
    }
    
    @Test
    public void test_T2_options() {
        System.out.println("@@@. testing --timeout ...");
        Class C = C.class;
        Main.main(C.getName() + " --debug --timeout=100 ");
    }
}