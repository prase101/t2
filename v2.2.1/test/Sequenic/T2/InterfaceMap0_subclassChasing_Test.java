package Sequenic.T2;

import org.junit.Test;
import static org.junit.Assert.*;
import SubclassChasing.Bla.* ;

public class InterfaceMap0_subclassChasing_Test {

    @Test
    public void test1() {
        System.out.println("@@@ check if T2 auto-find concrete impl-class when asked to create an instance of e.g. an interface...");
        Class C = SUT.class;
        TrFile.delete(C);
        Main.Junit(C.getName() + " --debug --silent --nmax=100 --lenexec=7 --violmax=3 --savegoodtr=3");
        // Main.Junit(C.getName() + " --debug --nmax=100 --lenexec=7");
        Debug.assertTrfNonEmpty(C);
    }
    
}
