package Sequenic.T2.Engines;
import java.io.* ;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author underdarkprime
 */
public class EngineWithTimeOutTest {

    public EngineWithTimeOutTest() {}

    static class Engine1 extends EngineWithTimeOut {

        public void mainloop() {
            int i = 0 ;
            while (true) {i++; i-- ; }
        }
        
    }
    
    static class Engine2 extends EngineWithTimeOut {

        public void mainloop() {
            System.out.println("Engine2...") ;
            assert false ; 
        }
        
    }

    @Test
    public void test1() {
        System.out.println("@@@ Test if time out will actually kick in...");
        EngineWithTimeOut E = new Engine1();
        E.timeOutPeriod = 3000 ;
        try { E.timedRun() ; }
        catch (Error e) { e.printStackTrace(); assert false ; }
    }
    
    @Test
    public void test2() {           
        System.out.println("@@@ Test if error thrown in/by the engine is indeed passed on...");
        EngineWithTimeOut E = new Engine2();
        E.timeOutPeriod = 3000 ;
        try { E.timedRun() ; assert false ; }  
        catch (Error e) { 
            //System.out.println("XXX") ; 
            assert e instanceof AssertionError ;
            e.printStackTrace();
        }
    }

}