package Sequenic.T2.Engines;

import Sequenic.T2.Debug ;
import Sequenic.T2.Msg.*;

import java.io.* ;
import org.junit.Test;
import static org.junit.Assert.*;

public class ReplayPlugTest {

	 public ReplayPlugTest() {}
	 
	 static public class C1 {

	        private int x = 0;
	        public C1 next = null;

	        public C1(int x0) {
	            x = x0;
	        }

	        public void m() {
	        }

	        public void ouch() {
	            assert false;
	        }
	   }
	 
	 @Test
	    public void test1() {
	        System.out.println("@@@ 3x violations and 1x non violation...");
	        BaseEngine engine = new BaseEngine(C1.class);
	        engine.maxNumViolations = 3;
	        engine.saveThisMany = 1;
	        
	        Debug.FlushDebugOutStream() ;
	        engine.out = Debug.pout ;
	        engine.reporters.setOutStream(Debug.pout) ;
	        engine.timedRun() ;
	        assert Debug.out.toString().contains("Saving 4 traces");
	        
	        //System.out.println(">>> " + Debug.out.toString()) ;
	        
	        ReplayToolPlug replayer = new ReplayToolPlug() ;
	        
	        replayer.engine = new Replay(C1.class.getName() + ".tr");
	        Debug.FlushDebugOutStream() ;
	        replayer.engine.out = Debug.pout ;
	        replayer.engine.reporters.setOutStream(Debug.pout);
	        //replayer.replayUpToThisMany = 4 ;
	        try { replayer.engine.load(); }
	        catch (Exception e) { e.printStackTrace(); assert false ; }
	        //System.out.println(">>>" + replayer.loadedTraces.traces.size()) ;
	        replayer.engine.selectTraces();
	        //System.out.println(">>>" + replayer.selected.traces.size()) ;
	        try {
 	          replayer.run() ;
	        }
	        catch (Violation e) {
	        	e.printStackTrace() ;
	        }
	        assert Debug.out.toString().contains("3 assertion violations");
	    }
	 
}
