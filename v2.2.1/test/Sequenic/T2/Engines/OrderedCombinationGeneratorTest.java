package Sequenic.T2.Engines;

import java.math.BigInteger;

import org.junit.Test;
import Sequenic.T2.Obj.Show;

public class OrderedCombinationGeneratorTest {

    static void printComb(int[] a) {
        System.out.print(">>> ");
        for (int i = 0; i < a.length; i++) {
            System.out.print("," + a[i]);
        }
        System.out.println("");
    }

    @Test
    public void test0() {
        	int n = 5 ;
        	int k = 3 ;
            OrderedCombinationGenerator gen = new OrderedCombinationGenerator(n,k) ;
            int[] r = new int[k] ;
            long N =  gen.getTotal() ;
            System.out.println(">>> number of combinations: " + N ) ;
            int i = 1 ;
            while (gen.hasMore()) {
            	gen.getNext(r) ;
            	System.out.println(">>> [" + i + "] " + Show.show(r)) ;
            	i++ ;
            }
        }
 
 
}