package Sequenic.T2.Obj;
import Sequenic.T2.* ;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class Show_Test2 {

    /**
     * Object generator for testing Show.
     */
    public static class ObjectToShow {

        Object obj = null;
        public ObjectToShow(ClonerTest.Graph2 u) { obj = u; }
        public ObjectToShow(ClonerTest.Graph2[] u) { obj = u; }
        public ObjectToShow(Collection u) { obj = u; }
    }
    
    @Test
    public void test() {
        System.out.println("@@@ Using T2 to auto-test Show ...");
        Class C = Show.class;
        Show.Debug = true ;
        TrFile.delete(C);
        Main.Junit(C.getName() + " --debug -n 20000 --exclfield --elemty=" + ObjectToShow.class.getName());
        //Debug.assertTrfEmpty(C);
    }
    
}
