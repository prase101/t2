package Examples;
import Sequenic.T2.* ;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author underdarkprime
 */
public class BinarySearchTreeTest {

    @Test
    public void test() {
        // practically 100%
        Main.Junit(BinarySearchTree.class.getName() + " --nmax=20000 --lenexec=7 --violmax=3 --savegoodtr=3") ;
    }

}