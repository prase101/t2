package Examples;
import Sequenic.T2.* ;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author underdarkprime
 */
public class SimpleTemporalSpecDemoTest {

   @Test
    public void test() {
        // Measured coverage: after increasing n and l to 20K/7, we get
        // practically 100%
        Main.Junit(SimpleTemporalSpecDemo.class.getName() + " --exclfield") ;
    }
}