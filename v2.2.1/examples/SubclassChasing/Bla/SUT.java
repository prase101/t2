package SubclassChasing.Bla;

public class SUT {

	public SUT() { super() ; }
	
	// we'll see if T2 can automatically find an implementation for a
	public void STEP(AInterface a) { 
		assert (a!=null) : "PRE" ;
		a.foo(99) ; 
	}
	
}
