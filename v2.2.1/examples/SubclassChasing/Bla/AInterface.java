package SubclassChasing.Bla;

/**
 * An example interface. It has sub-interface as well multiple classes
 * implementing it.
 */
public interface AInterface {

	public int foo(int x) ;
	
}
