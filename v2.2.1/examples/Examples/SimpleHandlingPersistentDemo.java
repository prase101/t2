package Examples;

import java.io.*;

/**
 * A simple example to demonstrate a test pattern for checking a class that
 * use a persistent component. The class below has a variable temperature and
 * a method log() that will log the temperature at that moment to a file.
 * The value will be <i>appended</i> to the file. To keep it simple, we fix
 * the log file; it's the file "mylog".
 * 
 * <p>The important thing to note here is that this file mylog is a persistent
 * component. When T2 generates a sequence, it my call log, and therefore write
 * to mylog, which may influence the next sequence (e.g. when the next sequence
 * does countLog).
 * 
 * <p>What we need to do is to make T2 resets the persistent components just before
 * it starts a new sequence. This can be done by subclassing Pool, and coding
 * the reset in the method reset of Pool.
 */
public class SimpleHandlingPersistentDemo {

    int temperature ;
    FileWriter fw ;

    public SimpleHandlingPersistentDemo() {
        try { fw = new FileWriter("mylog",true) ; }
        catch (Exception e) { }    
    }

    public void increaseTemperature() { temperature++ ; }

    public void log(){
       try { fw.write(":" + temperature) ; fw.flush() ; }
       catch (Exception e) { }
    }
    
    public int countLog() {
        // ... suppose we read the file mylog here and counts
        // how many times we have logged there
        //assert false ;
        return 0 ;
    }
    
    static public class MyPool extends Sequenic.T2.Pool {

        public MyPool() { super() ; }

        public void reset() {  
            // This will do to empty the file:
            try { FileWriter fw = new FileWriter("mylog") ; fw.close();} 
            catch (Exception e) { } ;
            super.reset();
        }
    }
    

    static public void main(String[] args) {
        // Calling T2 from here:
        Sequenic.T2.Main.main(SimpleHandlingPersistentDemo.class.getName() 
                + " --exclfield --pool=" + MyPool.class.getName());
    }
}
