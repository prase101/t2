package Sequenic.T2;
import Sequenic.T2.Engines.* ;
import Sequenic.T2.Msg.*;

import java.io.File;
import java.util.*;
import jargs.gnu.*;
import Sequenic.P2.* ;


/**
 * The T2 main tool. This is a console application. It is actually a mini
 * framework where one or more main functionalities can be plugged in.
 * Two standard functionalities are included, namely (automated) testing and 
 * replaying saved tests (regression).
 */
public class Main extends ToolPlug {

    static public class ToolDescriptor {

        public String selector;
        public String name;
        public ToolPlug tool;

        public ToolDescriptor(String s, String n, ToolPlug plug) {
            selector = s;
            name = n;
            tool = plug;
        }
    }
    public List<ToolDescriptor> tools = new LinkedList<ToolDescriptor> () ;
    public ToolDescriptor maintool = null ;
    // Derived variables:
    private String[] options = null ;
    // Options without the first element:
    private String[] options1 = null;

    public Main() {
        ToolDescriptor BaseEngine 
                =
                new ToolDescriptor("E","Base-engine", new BaseEngineToolPlug()) ;
        
        tools.add(BaseEngine);
        
        ToolDescriptor ReplayEngine
                =
                new ToolDescriptor("R","Replay-tool", new ReplayToolPlug()) ;
        
        tools.add(ReplayEngine) ;
        
        ToolDescriptor CombinatoricEngine
                =
                new ToolDescriptor("C","Combinatoric-engine", new CombinatoricEngineToolPlug()) ;
        
        tools.add(CombinatoricEngine) ;
        
        maintool = BaseEngine ;
    
    }
    
    private boolean isValidSelector(String sel) {
        boolean found = false;
        for (ToolDescriptor td : tools) {
            found = sel.equals("-" + td.selector);
            if (found) {
                break;
            }
        }
        return found;
    }

    private boolean isHelpRequest(String opt) {
        return opt.equals("--help");
    }

    public void configure(String[] args) {
        options = args;
        if (options.length > 1) {
            options1 = new String[options.length - 1];
            for (int i = 1; i < options.length; i++) {
                options1[i - 1] = options[i];
            }
        } else {
            options1 = new String[0];
        }
    }
    
    public void run() {
        if (options.length <= 0 || isHelpRequest(options[0])) {
            consoleHelp();
            return;
        }
        // Now #options > 0
        // This the the case of --selector or --selector --help
        if (isValidSelector(options[0]) &&
                (options.length == 1 
                || 
                (options.length == 2 && isHelpRequest(options[1])))) {
            for (ToolDescriptor td : tools) {
                if (options[0].equals("-" + td.selector)) {
                    td.tool.consoleHelp();
                    break;
                }
            }
            return;
        }

        // The case of either (1) <option>+ with no selector, or (2)
        // --selector <option>+
        // In case (1) pass ALL options to the main tool, else pass options1 :
        String[] optionsToPass = options;
        ToolDescriptor selectedTool = maintool;
        if (isValidSelector(options[0])) {
            for (ToolDescriptor td : tools) {
                if (options[0].equals("-" + td.selector)) {
                    optionsToPass = options1;
                    selectedTool = td;
                    break;
                }
            }
        }
        // Pass options to the selected tool for configuration:
        try {
            selectedTool.tool.configure(optionsToPass);
        } catch (CmdLineParser.IllegalOptionValueException e) {
            println("** " + e.getMessage());
            return;
        } catch (CmdLineParser.UnknownOptionException e) {
            println("** " + e.getMessage());
            return;
        }
        
        // Else options are valid :
        
        // Check if assertions are enabled:
        boolean assertsEnabled = false;
        assert assertsEnabled = true;  // Intentional side-effect!!!
        if(!assertsEnabled) {
        	println("** Warning: assertions are not enabled!");
        }
        
        selectedTool.tool.run();
    }

    public void consoleHelp() {
        println(Message.GREET);
        println(Message.BEGIN) ;
        println("GENERAL USE:\n");
        println("   java -ea -cp <classpaths> Sequenic.T2.Main <option>*\n");
        println("Specific uses depend on the options:");
        println("  (1) Printing this help: --help");
        println("  (2) Printing a tool's specific help: --toolSelector [--help]");
        println("  (3) Using a tool: --toolSelector <tool-specific-option>*\n");
        
        println("Tool Selectors:");

        for (ToolDescriptor td : tools) {
            println("  -" + td.selector + "  : " + td.name);
        }
        println(Message.END) ;

    }

    /**
     * Main, but no error is catched.
     */
    static private void main_(String[] args) {
        Main t2miniFramework = new Main();
        t2miniFramework.configure(args);
        t2miniFramework.run();
    }
    
    static public void main(String[] args) {
        try {
           main_(args) ;
        }
        catch (Violation e) { }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    /**
     * As main, but takes a single string as input, rather than an array of
     * strings. It will also swallow 'Violation' signal thrown by the engines,
     * but otherwise won't catch other errors thrown (so that when it is used
     * embedded in another application, the host application can choose how
     * to handle the errors.)
     */
    static public void main(String args) {
        try { main_(args.split("\\s")) ; }
        catch (Violation e) { }
    }
    
    /**
     * A Junit version of main. Currently it's just main_. It is very simplistic;
     * when the chosen engine finds a violation, it also throws an instance of
     * Violation. We won't catch this, so Junit is notified. However, currently
     * I don't yet how to pass on the details of violations found to Junit. So,
     * the user will still have to analyze the generated report manually :( 
     * I hope to improve this in the future. 
     */
    static public void Junit(String args) {
        main_(args.split("\\s")) ;
    }
    
    /**
     * This is another Junit variant of main. It will first check if a
     * save-file (containing saved test sequences from a previous run
     * of T2) exists. If no save-file exists, this will run T2 to generate
     * test sequences (as normal), passing to it the arguments in genargs.
     * 
     * <p>If a save-file exists, it will be loaded and replayed instead.
     * 
     * @param CUT  The target class.
     * 
     * @param trfile The name of the save-file (a .tr file) of test-sequences. 
     * If null then the default name CUTname.tr will be assumed.
     * 
     * @param genargs Other arguments to be passed to T2 for the tests generation
     * mode. Don't repeat the name of the CUT nor the name of the save-file.
     * 
     * @param replayargs Other arguments to be passed to the replay tool. Don't repeat
     * the name of the CUT not the name of the save-file.
     */
    static public void GenAndReplayJunit(Class CUT, 
    		String trfile, 
    		String genargs,
    		String replayargs) {
    	
    	if (trfile==null) {
    		trfile = CUT.getName() + ".tr" ;
    	}
    	
    	if (! trfile.endsWith(".tr")) trfile += ".tr" ;
    	
    	File file = new File(trfile) ;
    	if (file.exists()) {
    		Junit("-R " + trfile + " " + replayargs) ;
    	}
    	else {
    		Junit(CUT.getName() + " " + genargs + " --savefile=" + trfile) ;
    	}
    }
    
}
