package Sequenic.T2;
import java.io.* ;
import java.util.* ;
import Sequenic.T2.Msg.* ;

/**
 * An implementation of {@link Sequenic.T2.BaseDomain BaseDomain}, providing
 * values for primitive types, enumeration types, and String. This is T2's
 * default base domain. Its behavior
 * is as follows:
 *
 * <ol>
 * <p><li>If an instance of an enumeration type is requested, one will be
 * picked randomly.
 * 
 * <p><li>If an instance of a boolean or Boolean is requested, than true or
 * false will be chosen randomly.
 * 
 * <p><li>If an instance of a int-like numeric type (primitive or its wrapper) is
 * requested, a value between -3 upto (and exclusive) range-3 will be picked
 * randomly. The default of range is 7.
 * 
 * <p><li>If an instance of a float-like numeric type (primitive or its wrapper) is
 * requested, a value between -3 upto (and exclusive) range-3 will be picked
 * randomly. 
 *
 * <p><li>If an instance of a char is requested, one of the following will be
 * chosen randomly: 'a', 'A', '@' , '\n' , '\u0000', '\uFFFF'.
 * 
 * <p><li>If an instance of a string is requested, one of the following will be
 * chosen randomly: "",
 *		"croco",
 *		"TiGeR",
 *		"1",
 *		"line1 \n  line2",
 *		"\n \t",
 *		"~!@#$%^&*()_-+<>?,.:;'[]{}`",
 *		"\"quote me\"",
 *		"\"".
 * 
 * <p><li>If a value of a primitive type is requested, this base domain
 * always return a value of its wrapper type.
 *
 * <p><li>This base domain never returns a null.
 * </ol> 
 * 
 *  */
public class BaseDomainSmall extends BaseDomain {
	
	protected Random rnd = new Random() ;
	protected int range = 7 ;
	public int getRange() { return range; }
    
	protected char[] charsupply = { 'a' , 'A', '@' , '\n' , '\u0000', '\uFFFF' } ;
	protected String[] stringsupply = {
		"",
		"croco",
		"TiGeR",
		"1",
		"line1 \n  line2",
		"\n \t",
		"~!@#$%^&*()_-+<>?,.:;'[]{}`",
		"\"quote me\"",
		"\""
	} ;
	public char[] getCharsupply() { return charsupply ; }
    public String[] getStringsupply() { return stringsupply ; }
    
    
    
	// Introduce these variables at the object level as optimization:
	private int a  = 0 ;
	private float x = 0 ;
	private boolean b = false ;
	private Object[] res = new Object[1] ;
	
	public BaseDomainSmall() { }
	public BaseDomainSmall(int Range) { range = Range ; }
	
	
	public Object[] get(Class C) {
		a  = rnd.nextInt(range) - 3 ;
		x   = rnd.nextFloat()*range - 3 ;
		//System.out.println(">>" + a) ;
		//System.out.println(">>" + x) ;
		
		if (C == Integer.TYPE || C == Integer.class) { res[0] = new Integer(a) ; return res ; }
		if (C == Float.TYPE || C == Float.class) { res[0] = new Float(x) ; return res ; }
		if (C == Double.TYPE || C == Double.class) { res[0] = new Double(x) ; return res ; }
		if (C == Boolean.TYPE || C == Boolean.class) {
			b = rnd.nextBoolean() ;
			res[0] = new Boolean(b) ;
			return res ;
		}
		if (C == Byte.TYPE || C == Byte.class) { res[0] = new Byte((byte) a) ; return res ; }
		if (C == Short.TYPE || C == Short.class) { res[0] = new Short((short) a) ; return res ; }
		if (C == Long.TYPE || C == Long.class) { res[0] = new Long(a) ; return res ; }
		if (C == Character.TYPE || C == Character.class) {
                        //System.out.println("!!!") ;
                        res[0] = new Character(charsupply[rnd.nextInt(charsupply.length)]) ;
			return res ;
		}
        if (C == String.class) {
            res[0] = stringsupply[rnd.nextInt(stringsupply.length)] ;
            return res ;
        }
		if (C.isEnum()) {
			Object [] enumvals = C.getEnumConstants() ;
			res[0] = enumvals[rnd.nextInt(enumvals.length)] ;
			return res ; 
			} 
		return null ;
		} 
			
}
