package Sequenic.T2.DataGen;


/**
 * An asbtract class for generating a random integer between a given
 * interval [low..high). Note that it is *exclusive* high. An implementation
 * should implement the exact distribution.
 */
abstract public class RandomInt  {
    
    abstract public Integer nextValue() ;
    abstract public int getLow() ;
    abstract public int getHigh() ; 

}
