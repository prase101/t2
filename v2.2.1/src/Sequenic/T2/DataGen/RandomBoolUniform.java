package Sequenic.T2.DataGen;

import java.security.SecureRandom ;

/**
 * This generates a number r between 0..1 with a uniform distribution. If
 * low<=r<high, true is returned; else false.
 */
public class RandomBoolUniform extends RandomBool {

	private SecureRandom generator = new SecureRandom();
    
    public RandomBoolUniform(double treshold) {
    	//assert treshold>=0 ;
        low = 0;
        high = treshold;
    }
    
    public RandomBoolUniform(double l, double h) {
    	assert h>=l ;
        low = l ;
        high = h ;
    }

    public boolean nextValue() {
    	double r = generator.nextDouble() ;
    	return low<=r && r<high ;
    }
}
