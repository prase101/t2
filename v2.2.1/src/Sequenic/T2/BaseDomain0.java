package Sequenic.T2 ;
import java.io.* ;
import java.util.* ;
import Sequenic.T2.Msg.* ;
// import Sequenic.T2.Obj.* ;

/**
 * An implementation of {@link Sequenic.T2.BaseDomain BaseDomain}, providing
 * values for primitive types, enumeration types, and String. This
 * domain is finite.
 *
 * <p>The exact values in the domain: see source code.
 *
 * <p> This class also exports its subdomains, e.g. the subdomain of
 * integers, String, etc.
 *
 */
public class BaseDomain0 extends BaseDomain {

	public HashMap<Class,ArrayList<Serializable>> domain ;
	Random rnd ;

	/**
	 * Construct a domain of integers values (like 0,1,-1, etc). This
	 * domain is included in the default base domain.
	 */
	static public ArrayList<Serializable> IntegerDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add(0) ;
		dom.add(1) ;
		dom.add(-1) ;
		dom.add(2) ;
		dom.add(10) ;
		dom.add(-2) ;
		dom.add(-10) ;
		dom.add(9999) ;
		dom.add(-9999) ;
		dom.add(1900) ;
		dom.add(2000) ;
		dom.add(4) ;
		dom.add(28) ;
		dom.add(29) ;
		dom.add(Integer.MAX_VALUE) ;
		dom.add(Integer.MIN_VALUE) ;
		return dom ;
	}	
	
	/**
	 * Construct a domain of strings (like "","bob", "1234",
	 * etc). This domain is included in the default base domain.
	 */
	static public ArrayList<Serializable> StringDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add("") ;
		dom.add("bob") ;
		dom.add("Ans") ;
		dom.add("tigeR") ;
		dom.add("biRd") ;
		dom.add("friday blue") ;
		dom.add("friday   blue") ;
		dom.add("friday \n  blue") ;
		dom.add("fri@$& \n  &^#( blue") ;
		dom.add("1234") ;
		dom.add("~!@#$%^&*()_-+<>?,.:;'[]{}`") ;
		dom.add("a2 h$   k@") ;
		dom.add("\n") ;
		dom.add("\t") ;
		dom.add("\"hello\"") ;
		dom.add("\"") ;
		dom.add("\"\"") ;
		return dom ;
	}	

	static public  ArrayList<Serializable> BoolDomain() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add(true) ;
		dom.add(false) ;
		return dom ;
	}


	static public ArrayList<Serializable> ByteDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add((byte) 0) ;
		dom.add((byte) 1) ;
		dom.add((byte) -1) ;
		dom.add((byte) 2) ;
		dom.add((byte) 10) ;
		dom.add((byte) -2) ;
		dom.add((byte) -10) ;
		dom.add(Byte.MAX_VALUE) ;
		dom.add(Byte.MIN_VALUE) ;
		return dom ;
	}	
	
	static public ArrayList<Serializable> ShortDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add((short) 0) ;
		dom.add((short) 1) ;
		dom.add((short) -1) ;
		dom.add((short) 2) ;
		dom.add((short) 10) ;
		dom.add((short) -2) ;
		dom.add((short) -10) ;
		dom.add((short) 127) ;
		dom.add(Short.MAX_VALUE) ;
		dom.add(Short.MIN_VALUE) ;
		return dom ;
	}

	static public ArrayList<Serializable> LongDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add(0L) ;
		dom.add(1L) ;
		dom.add(-1L) ;
		dom.add(2L) ;
		dom.add(10L) ;
		dom.add(-2L) ;
		dom.add(-10L) ;
		dom.add(9999L) ;
		dom.add(-9999L) ;
		dom.add(1999999999999999999L) ;
		dom.add(-1999999999999999999L) ;
		dom.add(Long.MAX_VALUE) ;
		dom.add(Long.MIN_VALUE) ;
		return dom ;
	}	


	static public ArrayList<Serializable> CharDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add('a') ;
		dom.add('b') ;
		dom.add('z') ;
		dom.add('0') ;
		dom.add('9') ;
		dom.add('@') ;
		dom.add('\\') ;
		dom.add('\'') ;
		dom.add('/') ;
		dom.add('\"') ;
		dom.add('\n') ;
		dom.add('\u0000') ;
		dom.add('\uABCD') ;
		dom.add('\uFFFF') ;
		return dom ;
	}	

	static public ArrayList<Serializable> FloatDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add(0.0f) ;
		dom.add(1.0f) ;
		dom.add(-1.0f) ;
		dom.add(1.0f/3.0f) ;
		dom.add(1.0e20f) ;
		dom.add(-1.0e20f) ;
		dom.add(10.00000001f) ;
		dom.add(-10.00000001f) ;
		dom.add(Float.MAX_VALUE) ;
		dom.add(Float.MIN_VALUE) ;
		return dom ;
	}

	static public ArrayList<Serializable> DoubleDomain_1() {
		ArrayList<Serializable> dom = new ArrayList<Serializable>() ;
		dom.add(0.0) ;
		dom.add(1.0) ;
		dom.add(-1.0) ;
		dom.add(1.0/3.0) ;
		dom.add(1.0e20) ;
		dom.add(-1.0e20) ;
		dom.add(10.00000001) ;
		dom.add(-10.00000001) ;
		dom.add(Double.MAX_VALUE) ;
		dom.add(Double.MIN_VALUE) ;
		return dom ;
	}	
	
	
	/**
	 * Construct a default base domain.
	 */
	public BaseDomain0() {
		domain = new HashMap<Class,ArrayList<Serializable>>() ;
		rnd = new Random() ;
	
		ArrayList intdomain = IntegerDomain_1() ;
		domain.put((new Integer(0)) . getClass(), intdomain) ;
		domain.put(Integer.TYPE, intdomain) ;

		ArrayList booldomain = BoolDomain() ;
		domain.put((new Boolean(true)) . getClass(), booldomain) ;
		domain.put(Boolean.TYPE, booldomain) ;

		ArrayList bytedomain = ByteDomain_1() ;
		domain.put((new Byte((byte) 0)) . getClass(), bytedomain) ;
		domain.put(Byte.TYPE, bytedomain) ;
		
		ArrayList shortdomain = ShortDomain_1() ;
		domain.put(Short.class, shortdomain) ;
		domain.put(Short.TYPE, shortdomain) ;

		ArrayList longdomain = LongDomain_1() ;
		domain.put((new Long(0L)) . getClass(), longdomain) ;
		domain.put(Long.TYPE, longdomain) ;

		ArrayList chardomain = CharDomain_1() ;
		domain.put((new Character('0')) . getClass(), chardomain) ;
		domain.put(Character.TYPE, chardomain) ;

		ArrayList floatdomain = FloatDomain_1() ;
		domain.put((new Float(0f)) . getClass(), floatdomain) ;
		domain.put(Float.TYPE, floatdomain) ;

		ArrayList doubledomain = DoubleDomain_1() ;
		domain.put((new Double(0.0)) . getClass(), doubledomain) ;
		domain.put(Double.TYPE, doubledomain) ;

		domain.put(("") . getClass(), StringDomain_1()) ;	
	}

	/**
	 * @see Sequenic.T2.BaseDomain#get
	 */
	public Object[] get(Class C) {

		if (C.isEnum() && !domain.containsKey(C)) {

			// add values of C to the base domain:
			Object [] enumvals = C.getEnumConstants() ;
			ArrayList dom = new ArrayList() ;
			for (int i=0; i<enumvals.length; i++) dom.add(enumvals[i]) ;
			domain.put(C,dom) ;
			// then draw one randomly:
			if (enumvals.length > 0) {
				Object[] r = new Object[1] ;
				r[0] = enumvals[rnd.nextInt(enumvals.length)] ;
				return r ; 
			} 
		} 

		if (!domain.containsKey(C)) return null ;

		ArrayList dom = domain.get(C) ;
		if (dom.isEmpty()) return null ;
		Object[] r = new Object[1] ;

		r[0] = dom.get(rnd.nextInt(dom.size())) ;
		return r ;
	}


	// just used for testing below:
	private Object rndGet(Class C) {
		Object[] r = get(C) ;
		if (r==null) return null ; else return r[0] ;
	}

	/**
	 * Just for testing the class.
	 */
	public static void main(String[] args) {
		
		BaseDomain0 base = new BaseDomain0() ;
		System.out.println(base.rndGet(Integer.TYPE)) ;
		System.out.println(base.rndGet(Integer.TYPE)) ;
		System.out.println(base.rndGet((new Integer(0)) . getClass())) ;
		System.out.println(base.rndGet((new Integer(0)) . getClass())) ;

		try { System.out.println(base.rndGet(Class.forName("java.lang.Integer"))) ; }
		catch (Exception e) { System.out.println("ouch!") ; } 

		System.out.println(base.rndGet("" . getClass())) ;
		System.out.println(base.rndGet("" . getClass())) ;


	}

}