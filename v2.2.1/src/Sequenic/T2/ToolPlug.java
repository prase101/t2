/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2;

import java.util.*;
import jargs.gnu.*;
import Sequenic.P2.*;

/**
 * An abstract class defining the common interface for tool plugs. 
 * We can 'plug' classes to as component-tools to T2 main tool. This
 * is done via this plug class, so that from the perspective of the
 * main tool, they (the component tools) all have the same interface.
 */
abstract public class ToolPlug implements Runnable {

    // public Runnable tool ;
    /**
     * For parsing options.
     */
    public CmdLineParser parser = new CmdLineParser();
    /**
     * A list of this tool's supported options, complete with description etc.
     */
    public List<OptionDescriptor> optionDescs = new LinkedList<OptionDescriptor>() ;

    /**
     * A list of attributes of the tool that reflect how the tool has been
     * configured. Each attribute is a string of the form name = value.
     */
    public List<String> config = new LinkedList<String> () ;
    
    /**
     * To configure tool. This should also check if the given options
     * are valid. If they are invalid, the corresponding exception is
     * thrown.
     */
    abstract public void configure(String[] options)
            throws
            CmdLineParser.IllegalOptionValueException,
            CmdLineParser.UnknownOptionException;

    static public class OptionDescriptor {

        //public String shortOption;
        //public String longOption;
        public CmdLineParser.Option option;
        public String desc;
        public boolean multiple = false ;

        public String show() {
            String r = "" ;
            String shortOption = option.shortForm() ;
            String longOption = option.longForm() ;
            if (option instanceof CmdLineParser.Option.BooleanOption) {
                if (shortOption != null) {
                    r = r + "-" + shortOption;
                }
                if (shortOption != null && longOption != null) {
                    r = r + " | ";
                }
                if (longOption != null) {
                    r = r + "--" + longOption;
                }
            } else {
                if (shortOption != null) {
                    r = r + "-" + shortOption + " <val>";
                }
                if (shortOption != null && longOption != null) {
                    r = r + "  |  ";
                }
                if (longOption != null) {
                    r = r + "--" + longOption + "=<val>";
                }
            }
            if (multiple) r = "[ " + r + " ]+" ;
            return ("** " + r + "\n" + StringFormater.alignLeft(desc, 72, 8));
        }
    }

    /**
     * To add a new option to the parser and the options-list.
     */
    public OptionDescriptor addOption(
            CmdLineParser.Option option,
            String desc) {
        
        OptionDescriptor D = new OptionDescriptor() ;
        D.option = option  ;
        D.desc = desc ;
        optionDescs.add(D) ;  
        return D ;
        
    }
    
    public OptionDescriptor addOption(
            CmdLineParser.Option option,
            String desc,
            boolean multiplePossible) {

        OptionDescriptor D = addOption(option,desc) ;
        D.multiple = multiplePossible ;
        return D ;
    }
    
    /**
     * To run the tool.
     */
    abstract public void run();

    /**
     * To print help information to the console.
     */
    abstract public void consoleHelp();

    public void println(String s) { System.out.println(s); }
    /**
     * To print the list of supported options and their one-line helps.
     */
    public void consoleOptionsHelp() {
        println("OPTIONS:");
        
        for (OptionDescriptor d : optionDescs) {
            println(d.show());
        }
    }
    
    public void printConfig() {
        println    ("** Invoked CONFIGURATION ") ;
        for (String attrib : config) {
            println("   ** " + attrib) ;
        } ;
        println("**") ;
    }
}
