/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Engines;

import java.security.SecureRandom;
import java.util.* ;
import java.lang.reflect.* ;


/**
 * Note: currently methods that can't accept CUT as the receiver are
 * excluded from the scope.
 * 
 * @author wishnu
 */
public class CombinatoricSeqGenerator extends AbstractCombinatoricSeqGenerator {

	/**
	 * You need to pass the base-engine; it is needed so that we can read what
	 * its scope is.
	 * 
	 * @param generatorChoice To choose between permutative or cartesian sequence 
	 * generator.
	 */	
    public CombinatoricSeqGenerator(BaseEngine e) { 
        engine = e ; 
    }
    
	public int r = 3 ;
    public int duplicates = 2 ;
    protected Constructor[] cons_scope ;
    protected Object[] fields_and_methods_scope ;
    
    AbstractCombinationGenerator combGenerator ;
    
    public void init() {
    	
    	int NumOfCons = engine.consIPs.size() ;
    	assert NumOfCons>0 ;
        cons_scope = new Constructor[NumOfCons] ;
        int i = 0 ;
        for (Constructor con : engine.consIPs) {
            cons_scope[i] = con ;
            i++ ;
        }
        // Randomize the order of the constructors:
        Random rnd = new SecureRandom();
        int j ;
        Constructor tmp ;
        for (int k=0; k<NumOfCons; k++) {
        	i = rnd.nextInt(NumOfCons) ;
        	j = rnd.nextInt(NumOfCons) ;
        	tmp = cons_scope[i] ; cons_scope[i] = cons_scope[j] ;
        	cons_scope[j] = tmp ;
        }
        
        // Only limit to methods that can accept CUT as the receiver.
        List<Method> methods = engine.getMethodsCanAcceptCUTinRec() ;
        Map<Method,Method> specmap = engine.getMethodSpecs() ;
        int NumOfMF = engine.fieldsIPs.size() + methods.size() ;
        assert NumOfMF > 0 ;
        
        fields_and_methods_scope = new Object[NumOfMF] ;
        
        i=0 ;
        for (Method m : methods) {
            if (specmap.containsKey(m)) fields_and_methods_scope[i] = specmap.get(m) ;
            else fields_and_methods_scope[i] = m ;
            i++ ;
        } 
        for (Field f : engine.fieldsIPs) {
            fields_and_methods_scope[i] = f ;
            i++ ;
        }
        
     // Randomize the order of the fields and methods :
        Object tmp2 ;
        for (int k=0; k<NumOfMF; k++) {
        	i = rnd.nextInt(NumOfMF) ;
        	j = rnd.nextInt(NumOfMF) ;
        	tmp2 = fields_and_methods_scope[i] ; 
        	fields_and_methods_scope[i] = fields_and_methods_scope[j] ;
        	fields_and_methods_scope[j] = tmp2 ;
        }
        
        // combGenerator = new OrderedCombinationGenerator(fields_and_methods_scope.length,r) ;
        // Initializing counters:
        current_cons =  cons_scope.length ;
        current_dup = duplicates ;
    }
    
    /**
     * Call this after init to use the cartesian SeqenceGenerator
     * instead of OrderedCombinationGenerator.
     */
    void selectCombinatoricEngine(int engineChoice) {
    	if (engineChoice == PERMUTATIVE_ENGINE)
    		combGenerator = new OrderedCombinationGenerator(fields_and_methods_scope.length,r) ;
    	else 
    		combGenerator = new SequenceGenerator(fields_and_methods_scope.length,r) ;
    }
          
    static final int PERMUTATIVE_ENGINE = 0 ;
    static final int CARTESIAN_ENGINE = 1 ;
    
    private int current_cons ;
    private int current_dup ;
    private List current_seq = new LinkedList() ;
    
    private void get_new_mseq() {
        // Clear current sequence, and add a dummy first element
        current_seq.clear() ;
        current_seq.add(null);
        // Get a new combination and fill in the steps:
        int[] indices = new int[r] ;
        combGenerator.getNext(indices) ;
        for (int i=0; i<indices.length; i++) current_seq.add(fields_and_methods_scope[indices[i]]) ;
    }
    
    private boolean done() {
        return (current_cons+1 >= cons_scope.length)
                && (current_dup >= duplicates)
                && !combGenerator.hasMore() 
                ;
    }
    
    private boolean nextCombination() {
        return (current_cons+1 >= cons_scope.length)
                && (current_dup >= duplicates)
                && combGenerator.hasMore() 
                ;
    }
    
    private boolean nextConstructor() {
        return (current_cons+1 < cons_scope.length)
                && (current_dup >= duplicates)
                ;
    }
    
    private boolean nextDuplicate() {
        return current_dup < duplicates ;
    }
    
    
    @Override
    public List next_sequence() {
        
        if (done()) return null ;
        
        if (nextDuplicate()) {
            current_dup++ ;
            return current_seq ;
        }
        
        if (nextConstructor()) {
            current_cons++ ;
            current_dup=1 ;
            current_seq.set(0,cons_scope[current_cons]) ;
            return current_seq ;
        }
        
        if (nextCombination()) {
            current_cons = 0 ;
            current_dup = 1 ;
            get_new_mseq() ;
            current_seq.set(0,cons_scope[current_cons]) ;
            return current_seq ;
        }
        assert false ;
        return null ;
    }
    
    public int totalNumberOfCombinations() {
        return  combGenerator.getTotal() * duplicates * cons_scope.length ;
    }

}
