package Sequenic.T2.Engines;import jargs.gnu.*;

public class CombinatoricEngineToolPlug extends BaseEngineToolPlug {
    
    private CmdLineParser.Option numOfDuplicatesO = parser.addIntegerOption("dup");
    private CmdLineParser.Option cartesianO = parser.addBooleanOption("cartesian");
    
    public CombinatoricEngineToolPlug() {
        super() ;
        addOption(numOfDuplicatesO, "Number of duplicates per sequence.");
        addOption(cartesianO, "Generate cartesian/product sequences (rather than permutative).");
    }
    
    @Override
    public void configure(String[] options) 
                throws 
                CmdLineParser.IllegalOptionValueException, 
                CmdLineParser.UnknownOptionException {
         
        super.configure(options);
        
        // Plug a different sequence generator:
        CombinatoricSeqGenerator seqGen = new CombinatoricSeqGenerator(engine) ;
        seqGen.r = engine.maxExecLength-1 ;
        seqGen.duplicates = (Integer) parser.getOptionValue(numOfDuplicatesO,seqGen.duplicates) ;
        seqGen.init() ;
        if (parser.getOptionValue(cartesianO) != null) {
            seqGen.selectCombinatoricEngine(CombinatoricSeqGenerator.CARTESIAN_ENGINE) ;
            config.add("Using Cartesian sequence generator.") ;
        }
        else {
        	seqGen.selectCombinatoricEngine(CombinatoricSeqGenerator.PERMUTATIVE_ENGINE) ;
        	config.add("Using permutative sequence generator.") ;
        }
        engine.seqGenerator = seqGen ;
        config.add("Total number of combinations = " + seqGen.totalNumberOfCombinations()) ;
        config.add("Number of duplicates per sequence = " + seqGen.duplicates) ;
                 
     }
    

}
