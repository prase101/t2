


>>> 23-10-07; finding fault in spec.


wishnu@oss /cygdrive/d/workshop/TT/netbeansProjects/TT/build/classes
$ java -ea U2.T2.Obj.SelfTest

----------------------------------------------------
**  T2 Testing Tool for Java. Version 0.4, 2007.
----------------------------------------------------
** Creating test engine ...
Oct 23, 2007 12:43:48 PM U2.T2.RndEngine <init>
WARNING:
   ----
   ##PROBLEM
   U2.T2.Obj.Cloner does not specify a class invariant.
   ----
----------------------------------------------------
** Engine CONFIGURATION:
** Max. number of steps : 500
** Max. execution depth : 4
** Max. object depth (on creation) : 4
** Max. array/collection size : 5
** Prob. of updating field : 0.1
** Prob. of auto generation of NULL : 0.1
** Prob. of passing target obj as parameter : 0.4
** Prob. of trying pool before constuctor : 0.7
** CUT           : U2.T2.Obj.Cloner
** TYVAR0        : U2.T2.Obj.Cloner
** Pool          : U2.T2.Pool
** Base domain   : U2.T2.BaseDomain0
** Interface map : U2.T2.InterfaceMap0
----------------------------------------------------
** Testing Cloner ...
** Time elapsed: 109 ms

** 1 VIOLATIONS found.
----------------------------------------------------
** total attempted execution steps : 68
** total generated executions      : 14
** number of irrelevant checks     : 0
----------------------------------------------------


** Violating trace [1] :
  ** CREATING target object.
    ** Return value:
       (U2.T2.Obj.Cloner) @ 0
  ** STEP 1.
  ** Calling method cloneIt_spec with:
    ** Receiver: target-obj
    ** Arg [0:]
       (U2.T2.Obj.Cloner) @ 1
    ** Target object after the step:
$
  ** STEP 2.
  ** Calling method cloneIt_spec with:
    ** Receiver: target-obj
    ** Arg [0:]
       NULL
    ** Target object after the step:
       (U2.T2.Obj.Cloner) @ 0
  xx INTERNAL ERROR!
  ** Strack trace:
java.lang.NullPointerException
        at U2.T2.Obj.Cloner.cloneIt_spec(Cloner.java:47)
        at sun.reflect.GeneratedMethodAccessor2.invoke(Unknown Source)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
        at java.lang.reflect.Method.invoke(Unknown Source)
        at U2.T2.Trace.exec(Trace.java:757)
        at U2.T2.Trace.report(Trace.java:851)
        at U2.T2.RndEngine.report(RndEngine.java:601)
        at U2.T2.RndEngine.RndTest(RndEngine.java:685)
        at U2.T2.RndEngine.RndTest(RndEngine.java:746)
$
        at U2.T2.Obj.SelfTest.Test_Cloner(SelfTest.java:36)
        at U2.T2.Obj.SelfTest.main(SelfTest.java:40)
  -----------------------------------
** Saving traces...
** DONE.
