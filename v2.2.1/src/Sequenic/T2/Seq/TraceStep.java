/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;

import Sequenic.T2.Pool;
import Sequenic.T2.export.Visitor;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * An object of this class is a meta representation of a test-step in T2.
 * When executed it will e.g. update a specified field, or call a method.
 */
public abstract class TraceStep extends METARUN {
	
	private static final long serialVersionUID = 1L;

    /**
     * Obtain the name of the step, as far as this makes sense.
     */
    public abstract String getName();


    /**
     * Execute this trace step, and produce reports as well if reporters is
     * not null.
     */
    public abstract ExecResult exec(Class CUT, Pool pool, 
            Object targetObj, 
            int stepNumber,
            List<Method> classinvs, 
            ReportersPool reporters );

    
    
    /**
     * As the other exec, but without reporting.
     */
    public ExecResult exec(Class CUT, Pool pool, 
            Object targetObj, 
            int stepNumber,
            List<Method> classinvs
            ) {
        
        ExecResult res = exec(CUT, pool, targetObj, stepNumber, classinvs, ReportersPool.NULLreporter);
        return res;
    }


	public void visit(Visitor visitor) {
		visitor.visit(this);
	}
}
