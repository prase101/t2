/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

import Sequenic.T2.Obj.*;
import Sequenic.T2.Pool;
import Sequenic.T2.Msg.*;
import Sequenic.P2.* ;
import java.lang.reflect.*;
import java.io.*;
import Sequenic.T2.Engines.Util ;
import java.util.List;

/**
 * This represents a test-step where we update a field of a target object.
 */
public class UPDATE_FIELD extends TraceStep {
	
	private static final long serialVersionUID = 1L;

    public Field field;
    public MkValStep val;

    /**
     * @param f The field to update.
     * @param v A MkVal step producing a new value for the field f.
     */
    public UPDATE_FIELD(Field f, MkValStep v) {
        field = f;
        val = v;
    }

    public String getName() {
        return field.getName();
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        try {
            Class C = Util.classOf((String) stream.readObject());
            String fieldName = (String) stream.readObject();
            // field = C.getField(fieldName) ;
            field = Util.getField(C, fieldName);
            val = (MkValStep) stream.readObject();
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new IOException();
        }
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        try {
            stream.writeObject(field.getDeclaringClass().getName());
            stream.writeObject(field.getName());
            stream.writeObject(val);
        } catch (Exception e) {
            throw new IOException();
        }
    }

    /**
     * @see Sequenic.T2.Seq.TraceStep#exec
     */
    public ExecResult exec(Class CUT, Pool pool, 
            Object targetObj,
            int stepNumber,
            List<Method> classinvs, 
            ReportersPool reporters) {

        SlotsInjector.injectAtTestStep(targetObj, this);

        field.setAccessible(true);
        ExecResult result = new ExecResult(CUT,this,targetObj);
        Object newFieldVal = null ;

        // System.out.println(">" + step_.field.getName()) ;
        // System.out.println(">" + MkVal(pool,step_.val)) ;
        // System.out.println("+" + targetObj) ;
        try {
            newFieldVal = val.exec(CUT,pool);
            result.args = new Object[] { newFieldVal } ;
            assert (targetObj != null);
            field.set(targetObj, newFieldVal);
            result.execClassInv(classinvs, targetObj);
        } 
        catch (InvocationTargetException e) {
        	// First mark argument failure:
            result.argumentFailure = e.getCause();  
            // Then categorize the exception:
            // result.checkException(e);
        }   
        catch (Exception e) {
            throw new T2Error("Fail to update field " + this , e);
        }
        
        // Reporting, unless if NULLreporter is used:
        reporters.reportStep(result,stepNumber) ;
        
        return result;
    }
    
    @Override
    public String toString() {
        return "UPDATE_FIELD, " + field.getName() + " with " 
                + 
                StringFormater.indentButFirst(val.toString(),4) ;
    }
}
