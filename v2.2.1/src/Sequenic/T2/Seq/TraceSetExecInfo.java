/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

/**
 * An object of this class is just a record to hold various statistics
 * recorded when executing a trace set. It is maining used for reporting.
 */
public class TraceSetExecInfo {

    public int totNumOfSteps = 0;
    public int numOfTraces = 0;
    /**
     * Number of meaningless traces generated.
     */
    public int numOfIrrelevantChecks = 0;
    /**
     * Meaningless trace due to violation to PRE
     */
    public int numOfPreCondViolations = 0;
    /**
     * Meaningless trace due to APPMODEL violation.
     */
    public int numOfAppModelViolations = 0;
    /**
     * Real errors/violations.
     */
    public int numOfViolations = 0;
    /**
     * Violation due to assert violation which is not PRE nor APPMODEL
     */
    public int numOfAssertViolation = 0;
    /**
     * Violation due class invariant.
     */
    public int numOfClassInvViolation = 0;
    /**
     * Number of times class-invariants throw an excetions.
     */
    public int numOfClassInvException = 0;
    /**
     * Violation due to casting Error or runtime exception.
     */
    public int numOfErrorViolation = 0;
    
    /**
     * The number of complete/full traces.
     */
    public int numOfFullTraces = 0 ;
    
    /**
     * Time in ms. It should be the time used to run a set of tests.
     */
    public long time = 0;
    
    /**
     * First violation found. 
     */
    public Throwable firstViolation = null ;

    private void setFirstViolation(Throwable e) {
    	if (firstViolation!=null) return ;
    	firstViolation = e ;
    }
    
    /**
     * To accumulate the result of a test-step in this trace-set info.
     */
    public void accumulateResult(ExecResult stepResult) {
        // Check ASM violation first!! 
        // (No, the order should not matter; it will catch whatever the
        // first violation is)
        if (stepResult.isAsmViolating()) {
            // irrelavant step
            numOfIrrelevantChecks++;
            if (stepResult.precViolation != null) {
                numOfPreCondViolations++;
                return;
            }
            if (stepResult.appmodViolation != null) {
                numOfAppModelViolations++;
                return;
            }
        }
        if (stepResult.isReqViolating()) {
            numOfViolations++;
            if (stepResult.classinvViolation) {
                numOfClassInvViolation++;
                setFirstViolation(new AssertionError("Class-invariant violation!"));
                return;
            }
            if (stepResult.postcViolation != null) {
            	setFirstViolation(stepResult.postcViolation) ;
                numOfAssertViolation++;
            }

            if (stepResult.internalError != null) {
            	setFirstViolation(stepResult.internalError) ;
                numOfErrorViolation++;
            }
            if (stepResult.classinvExecptionalViolation != null) {
            	setFirstViolation(stepResult.classinvExecptionalViolation) ;
                numOfClassInvException++;
            }
        }
    }
}
