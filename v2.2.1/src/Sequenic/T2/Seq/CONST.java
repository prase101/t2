/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;

import java.io.IOException;

import Sequenic.P2.StringFormater;
import Sequenic.T2.Pool;
import Sequenic.T2.Engines.Util;
import Sequenic.T2.Msg.T2Error;
import Sequenic.T2.Obj.Cloner;
import Sequenic.T2.export.Visitor;


/**
 * This represents a MkVal-step in which an object is 'created' by picking a
 * 'constant', e.g. from the {@link Sequenic.T2.BaseDomain base domain}.
 * The object used as 'constant' is remembered in this step.
 *
 */
public class CONST extends MkValStep  {

	private static final long serialVersionUID = 1L;
	
    public Object val;
    /**
     * When val is null, then we can't ask via reflection what it
     * class was; so we'll also keep track the class as well. This
     * field is only setup when val is null:
     */
    public Class C;

    /**
     * @param v The object used as the constant supplied by this step.
     */
    public CONST(Object v, Class E) {
        val = v;
        C = E;
    }

    public Object exec(Class CUT, Pool pool) {
        Object v = null;
        try {
            v = Cloner.clone(val);
        } catch (Exception e) {
            throw new T2Error("Fail to clone. Perhaps because Base Domain produces unclonable objects.", e);
        }
        return v;
    }

    public boolean isNull() {
        return val == null;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream)
			throws IOException, ClassNotFoundException
		{
			try {
				C = Util.classOf((String) stream.readObject()) ;
                                //System.out.println("xxx " + C.getName()) ;
				String isNull = (String) stream.readObject() ;
				if (isNull.equals("null")) val = null ;
				else val = stream.readObject() ;
                                //System.out.println("xxx ") ;
			}
			catch (ClassNotFoundException e) { throw e ; }
			catch (Exception e) {throw new IOException() ; } 
		}
    
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        try {
            if (val == null) {
            	//System.out.println("DEBUG CONST writeObject 1");
                //System.out.println("xxx") ;
                stream.writeObject(C.getName());
                //System.out.println("DEBUG CONST writeObject 2");
                stream.writeObject("null");
                //System.out.println("DEBUG CONST writeObject 3");
            } else {
                //System.out.println("yyy " + val.getClass().getName()) ;
                stream.writeObject(val.getClass().getName());
                stream.writeObject("not null");
                stream.writeObject(val);
            }
        } catch (Exception e) {
            throw new IOException();
        }
    }
    
    public String toString() {
        if (val==null) { 
            return "CONST null" ;
        } else if(val instanceof String) {
        	return "CONST <String>"; // TODO filter unprintable chars and show real value?
        } else {
            return "CONST " + val; //StringFormater.indentButFirst(val.toString(),4) ;
        }
    }
    
    @Override
    public void visit(Visitor visitor) {
		visitor.visit(this);
	}
}