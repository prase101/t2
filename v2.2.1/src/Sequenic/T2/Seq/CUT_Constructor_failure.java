package Sequenic.T2.Seq;
import java.lang.reflect.* ;

/**
 * Will be thrown by a test step execution if the step calls a CUT constructor,
 * and it violates a non-assumptive assertion.
 */
public class CUT_Constructor_failure extends InvocationTargetException {

    public CREATE_OBJECT step ;
    
    public CUT_Constructor_failure(Throwable cause, CREATE_OBJECT s) {
        super(cause) ;
        step = s ;
    }
    
}
