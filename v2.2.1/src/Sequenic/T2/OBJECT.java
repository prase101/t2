package Sequenic.T2;

/**
 * Just a minimal class which can be used as an abstract representation of
 * 'object' during the testing. When the --elemty option of T2 is not set,
 * then T2 will default it to this class.
 */
public class OBJECT {

}
