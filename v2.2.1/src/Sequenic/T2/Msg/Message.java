package Sequenic.T2.Msg ;
import Sequenic.P2.* ;
import java.io.* ;
//import java.util.logging.* ;

/**
 * Utility for producing standards messages in the T2 tool.
 */

public class Message {
	
	// -----------------------------------------------------------------
	// PROMPT like
	// -----------------------------------------------------------------
	/**
	 * Just a T2 greeting message.
	 */
	static public final String SVNVERSION = "#svnversion 452#";
	static public final String GREET =
		"\n+++\n+++ T2 Version 2.2.1, 2013 "+ SVNVERSION ;
	
	static public final String BEGIN = "---" ;
	static public final String END   = "---" ;
        
}