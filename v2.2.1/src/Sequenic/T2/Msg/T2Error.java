package Sequenic.T2.Msg ;

/**
 * This is to be thrown by a test engine when it thinks it is unsafe
 * to continue its operation. The engine should exit when it throws
 * this exception.
 */
public class T2Error extends Error {

	public static final String INCOMPLETENESS = "INCOMPLETE CODE." ;
	public static final String ABORT = "ABORT." ;
	public static final String BUG = "BUG?" ;
        
        public T2Error(String code, String msg) {
            super("## T2 " + code + "\n## " + msg) ;
        }
        
	public T2Error(String msg) { this(ABORT,msg) ; }
        
	public T2Error(String code, String msg, Throwable cause) { 		
		super("## T2 " + code + "\n## " + msg, cause) ; 
	}
        
	public T2Error(String msg, Throwable cause) { 		
		this(ABORT,msg,cause); 
	}

}