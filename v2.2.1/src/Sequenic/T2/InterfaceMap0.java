package Sequenic.T2;
import java.util.* ;
import java.lang.reflect.* ;

import org.reflections.* ;

/**
 * An predefined implementation of InterfaceMap.
 */
public class InterfaceMap0 extends InterfaceMap {

    HashMap<Class,Class> map ;
    Random rnd = new Random() ;
    HashMap<String,Reflections> reflectionsMap ;
    
    
	/**
	 * Creating an empty interface map.
	 */
	public InterfaceMap0() { 
        map = new HashMap<Class,Class>() ; 
        reflectionsMap = new HashMap<String,Reflections>() ;
    	
		try { map.put(Class.forName("java.lang.Comparable"),
                Class.forName("java.lang.Integer")) ;
		}
		catch (Exception e) { } 
		
		try { map.put(Class.forName("java.lang.Number"),
                Class.forName("java.lang.Integer")) ;
		}
		catch (Exception e) { }

		try { map.put(Class.forName("java.util.Collection"),
                Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.List"),
				Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Queue"),
				Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Set"),
				Class.forName("java.util.HashSet")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Map"),
				Class.forName("java.util.HashMap")) ;
		}
		catch (Exception e) { } 
    
    }

	/**
	 * Obtain an implementation of an interface I. Return null if it
	 * can't find I or an implementation of it in the interface map.
	 */
	public Class getImplementation(Class I) { 
	    Class impl = map.get(I) ;
	    if (impl != null) return impl ;
	    else return getImplementationClassThroughReflection(I) ;
	}
	
	/**
	 * Get the Reflection-data for a given package-path from the reflection-map.
	 * If it cannot be found, one will be created. This is to avoid such data
	 * to be built repeatedly.
	 */
	private Reflections getReflections(String packageName) {
		Set<String> keys = reflectionsMap.keySet() ;
		String match = null ;
		for (String p : keys) {
			if (p.equals(packageName) ||  packageName.startsWith(p + ".")) {
				match = p ; break ;
			}
		}	
		Reflections r = reflectionsMap.get(match) ;
		if (r == null) {
			r = new Reflections(packageName) ; 
			reflectionsMap.put(packageName, r);
		}
		return r ;
	}
	
	private boolean isInterfaceOrAbstract(Class c){
		return c.isInterface() ||  Modifier.isAbstract(c.getModifiers()) ;
	}
	 
	/**
	 * Check if a given class has a public constructor. If it is an interface
	 * or an abstract class, false is returned.
	 */
	private boolean hasConstructor(Class c) {
		if (isInterfaceOrAbstract(c)) return false ;
		Constructor[] constructors = c.getConstructors() ;
		if (constructors==null) return false ;
		for (int i=0; i<constructors.length; i++) {
			Constructor constr = constructors[i] ;
			if (Modifier.isPublic(constr.getModifiers())) return true ;
		}
		return false ;	
	}
	
	
	/**
	 * If c has no constructor (e.g it is an interface or abstract), this
	 * method will try to find a subclass/implementation with at least 
	 * one public constructor.
	 * If more than one such implementations can be found, one is selected
	 * randomly.
	 * 
	 * If c is an array, no subtype will be returned as array is handlerd
	 * separately by T2 engine.
	 */
	protected Class getImplementationClassThroughReflection(Class c) {
		if (c.isArray()) return null ;
		if (hasConstructor(c)) return null ;
		//System.out.println("## " + c.getName()) ;
		String pckName = c.getPackage().getName() ;
		Reflections r = getReflections(pckName) ;
		Set subTypes = (Set) r.getSubTypesOf(c);
		List<Class> subClassesWithConstructors = new LinkedList<Class>() ;
		for (Object d : subTypes) {
			Class d_ = (Class) d ;
			if (hasConstructor(d_)) subClassesWithConstructors.add(d_) ;
		}
		if (subClassesWithConstructors.isEmpty()) return null ;
		int K = rnd.nextInt(subClassesWithConstructors.size()) ;
		return subClassesWithConstructors.get(K) ;	
	}


}
