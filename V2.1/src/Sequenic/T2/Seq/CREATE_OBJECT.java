/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

import Sequenic.T2.Pool;
//import Sequenic.T2.Obj.*;
import Sequenic.T2.Msg.*;
import Sequenic.P2.* ;
import Sequenic.T2.Engines.Util ;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * This represents a MkVal-step in which an object is created by calling a
 * constructor P. The constructor will be remembered in this step.
 * We also keep track of the MkVal steps needed to create the
 * objects needed as the parameters for P.
 */
public class CREATE_OBJECT extends MkValStep {

	private static final long serialVersionUID = 1L;
	
    public Constructor con;
    public MkValStep[] params;
    /**
     * Set this to -1 if the index is still unknown.
     */
    public int indexOfNewObject;
    
   /**
     * @param c   The constructor used in this step.
     * @param ps  MkVal steps needed to generate parameters for c.
     * @param index The Pool-ID of the newly created object.
     */
    public CREATE_OBJECT(Constructor c, MkValStep[] ps, int index) {
        con = c;
        params = ps;
        indexOfNewObject = index;
    }

    /**
     * Execute the step. This is the worker-exec. The "args" parameter will be
     * filled with the actual arguments passed to the constructor.
     */
    
    private Object exec(Class CUT, Pool pool, List<Object> arglist) throws InvocationTargetException {
        
        if (arglist != null) arglist.clear() ;
        Object[] args = new Object[params.length];
        for (int i = 0; i < params.length; i++) {
            args[i] = params[i].exec(CUT,pool);
            if (arglist != null) arglist.add(args[i]) ;
        }
        Object newobject = null;
        con.setAccessible(true);

        try {
            newobject = con.newInstance(args);
        } 
        catch (InvocationTargetException e) { 
            if (con.getDeclaringClass() == CUT) throw new CUT_Constructor_failure(e.getCause(),this) ;
            else throw e ;
        }
        catch (Exception e) {
            // System.out.println("## " + step_.con.getName()) ;
            // System.out.println("## " + Show.show(e)) ;
            throw new T2Error("Fail to create object via constructor " + this, e);
        }
        int index = pool.put(newobject);
        if (indexOfNewObject == -1) {
            indexOfNewObject = index;
        }
        assert (index == indexOfNewObject);
        return newobject;
    }
    
    public Object exec(Class CUT, Pool pool) throws InvocationTargetException {
        return exec(CUT,pool,null) ;
    }
    
    /**
     * Like exec, but special for producing target object. 
     */
            
    public ExecResult MkTargetObject(Class CUT, 
            Pool pool, 
            List<Method> classinvs,
            ReportersPool reporters) 
    {
        ExecResult result = new ExecResult(CUT,this,null) ;
                
        // Execute the step:
        try { 
            List arglist = new LinkedList() ;
            result.targetObj =  exec(CUT,pool,arglist);
            if (arglist.size() > 0) {
                result.args = new Object[arglist.size()] ;
                for (int i=0 ; i<result.args.length; i++) {
                    result.args[i] = arglist.get(i) ;
                }
            }
            SlotsInjector.injectAtTestStart(result.targetObj, this);      
            result.execClassInv(classinvs, result.targetObj);
            result.returnedObj = result.targetObj ;
        }
        catch (InvocationTargetException e) {
            //result.argumentFailure = e.getCause() ;
            result.checkException(e);
        }

        // Report, unless NULLreporter is used:
        reporters.reportStep(result,0);
        return result;
    }
    
    
    

    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        try {
            stream.writeObject(con.getDeclaringClass().getName());
            Class[] paramTypes = con.getParameterTypes();
            String[] paramTypes_ = new String[paramTypes.length];
            for (int i = 0; i < paramTypes.length; i++) {
                paramTypes_[i] = paramTypes[i].getName();
            }
            stream.writeObject(paramTypes_);
            stream.writeObject(params);
            stream.writeInt(indexOfNewObject);
        } catch (Exception e) {
            throw new IOException();
        }
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        try {
            Class C = Util.classOf((String) stream.readObject());
            String[] paramTypes_ = (String[]) stream.readObject();
            Class[] paramTypes = new Class[paramTypes_.length];
            for (int i = 0; i < paramTypes_.length; i++) {
                paramTypes[i] = Util.classOf(paramTypes_[i]);
            }
            // con = C.getConstructor(paramTypes) ;
            con = Util.getConstructor(C, paramTypes);
            params = (MkValStep[]) stream.readObject();
            indexOfNewObject = stream.readInt();
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (Exception e) {
            //e.printStackTrace();
            throw new IOException();
        }
    }

    static public CREATE_OBJECT META_Construct_an_Object() {
        Class[] paramtypes = new Class[0];
        CREATE_OBJECT result = null;
        try {
            result =
                    new CREATE_OBJECT(Object.class.getDeclaredConstructor(paramtypes),
                    new MkValStep[0],
                    -1);
        } catch (Exception e) {
        }
        ;
        return result;
    }
    
    public String toString() {
        String result = "CREATE_OBJECT, " + con.getName() ;
        
        String paramsTxt = "" ;
        for (int i=0; i<params.length; i++) {
            paramsTxt = paramsTxt + params[i] ;
            if (i<params.length-1) paramsTxt = paramsTxt + ",\n" ;
        }
        result = result + " (" 
                + StringFormater.indentButFirst(paramsTxt,4)
                +")" ;
        
        return result ;
    }
    
}
