/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

import Sequenic.T2.Pool;
//import Sequenic.T2.Obj.*;
import Sequenic.T2.Msg.*;
//import Sequenic.T2.Reflection.*
import java.util.*;
import java.lang.reflect.*;

//import java.io.*;
/**
 * A trace set is just a set of traces, targetting the same target class (the
 * class under the test). This class provides some basic utitlities like
 * for executing the tests in a trace set, and reporting the result.
 */
public class TraceSet {

    /**
     * The target class (class under the test).
     */
    public Class CUT;

    /**
     * The set of traces.
     */
    public LinkedList<Trace> traces = new LinkedList<Trace>();

    /**
     * A link to a pool with which the traces in this trace set were created.
     * It is important to link to exactly the same pool; else the traces
     * will be produce entirely different executions. Furthermore it is
     * implicitly assumed here that all the traces in this trace set is thus
     * created with the same pool (so a set of traces with different pools
     * cannot be accomodated with this class).
     */
    public Pool pool;

    /**
     * This is an option that is active when executing a trace set.
     * When it is true (default) of intermediate steps in a trace will also be
     * reported when the trace is reported.
     * Else only the creation of the target object and the last two
     * steps will be reported.
     */
    //public boolean printIntermediateStepsOption = true;

    /**
     * This is an option that is active when executing a trace set.
     * The default is that all violating traces will be reported. However
     * we can make the executor to report only the first N violations.
     * The entire trace set is still executed, and the exact number of
     * violations will be reported in the end.
     *
     * <p>When this option is -1, then all violations will be reported.
     *
     * <p>The default is 1.
     */
    //public int maxNumViolationOption = 1;

    public TraceSet(Class C, Pool p, LinkedList<Trace> ts) {
        CUT = C ;
        pool = p;
        traces = ts;
    }

    /**
     * Return a new trace set consisting of all traces from this
     * set which are marked as violating.
     */
    public TraceSet getViolatingTraces() {
        LinkedList<Trace> newSet = new LinkedList<Trace>();
        for (Trace sigma : traces) {
            if (sigma.violating) newSet.add(sigma);
            }
        return new TraceSet(CUT,pool,newSet) ;
    }

    /**
     * Return a new trace set consisting of all traces from this
     * set which are marked as diverging.
     */
    public TraceSet getDivergingTraces() {
        LinkedList<Trace> newSet = new LinkedList<Trace>();
        for (Trace sigma : traces) {
            if (sigma.diverging) newSet.add(sigma);
            }
        return new TraceSet(CUT,pool,newSet) ;
    }
    
    /**
     * This method will execute in principle all traces in this set. If 
     * reporters is not null, then it will also produce reports. The method
     * will STOP after executing a trace suspected to be diverging
     *
     * <p> This method will return a TraceSetExecInfo, containing statistics
     * about the executions.
     * 
     * @param maxNumViolation If positive will force the method to stop after
     * encountering this much violations.
     * 
     * @param dont_execute_last_diverging_step If set to true will cause the
     * following. When executing a trace marked as diverging, this method will
     * assume the last step to be diverging; so it will not execute it. 
     */
    public TraceSetExecInfo exec(List<Method> classinvs, 
            boolean dont_execute_last_diverging_step,
            boolean printIntermediateStepsOption,
            int maxNumViolation,
            ReportersPool reporters) {

        TraceSetExecInfo info = new TraceSetExecInfo();
        TraceExecInfo traceResult;

        boolean stopAfterNviolations = maxNumViolation>0 ;

        //reporters.reportTestSetBegin(CUT,Message.GREET);

        info.time = System.currentTimeMillis();

        for (Trace sigma : traces) {

            assert (sigma != null  &&  sigma.creation != null) ;
            // Execute sigma:
            //System.out.println(">>>" + maxNumViolationOption) ;
            if (dont_execute_last_diverging_step && sigma.diverging)
                traceResult = sigma.exec(CUT, pool, classinvs, true, printIntermediateStepsOption, reporters);
            else
                traceResult = sigma.exec(CUT, pool, classinvs, false, printIntermediateStepsOption, reporters);
            info.accumulateResult(traceResult.lastStepResult);
            info.totNumOfSteps = info.totNumOfSteps + sigma.trace.size();
            info.numOfTraces++;
            // Immediately break after suspecting divergence:
            if (traceResult.new_diverging_flag) break ;
            if (traceResult.new_violating_flag) {
                maxNumViolation-- ;
                // Also break if max number of violations was set, and is reached:
                if (stopAfterNviolations && maxNumViolation<=0) break ;
            }
        }

        info.time = System.currentTimeMillis() - info.time;

        //reporters.reportTestSetEnd(CUT,info,"");

        return info ;
    }

}
