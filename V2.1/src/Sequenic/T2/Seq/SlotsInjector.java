/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Seq;

import java.lang.reflect.*;

/**
 * This class provides a set of static methods that can be used by a test
 * engine to 'inject' information into target objects. Primarily they are used
 * to inject information only available during the execution of a test into 
 * its target object, e.g. this can be the name of the last method called,
 * or even the entire test so far. The class of the target object may in 
 * turn use such additional information to express certain specifications.
 * 
 * <p>Information will put into 'slots', which are just some pre-defined 
 * variables in the target object, like aux_laststep. If these slots are
 * declared in the target object, they will be filled.
 * 
 * <p>A new kind of injection can be added by coding it in this class.
 *
 * @author underdarkprime
 */
public class SlotsInjector {

    /**
     * Inject the name of the method called in the given step, or the name
     * of the field it updates. This will be injected to a variable called
     * aux_laststep.
     */
    private static void update_aux_laststep(Object targetObj, String step) {
        try {
            Field laststep_hook = targetObj.getClass().getDeclaredField("aux_laststep");
            laststep_hook.setAccessible(true);
            String step_ = step;
            if (step.endsWith("_spec")) {
                step_ = step.substring(0, step.length() - 5);
            }
            laststep_hook.set(targetObj, step_);
        } catch (Exception e) {
        }
    }

    /**
     * Inject the test sequence so far into the target object. This will be
     * injected in a variable called aux_trace.
     */
    private static void update_aux_trace(Object targetObj, METARUN step) {
        try {
            
            Field sigma_hook = targetObj.getClass().getDeclaredField("aux_trace");
            sigma_hook.setAccessible(true);
 
                Trace sigma = (Trace) sigma_hook.get(targetObj) ;
                //System.out.println("....") ;
                if (step instanceof MkValStep) {
                    sigma = new Trace() ;
                    sigma.creation = (MkValStep) step ;
                }
                else {
                    //System.out.println("INJECTING " + step) ;
                    sigma.trace.add((TraceStep) step) ;
                }
                sigma_hook.set(targetObj, sigma);
            }
            catch (Exception e) {
        }
    }

    /**
     * Execute step-injectors at the end of the given test-step.
     */
    static void injectAtTestStep(Object targetObject, TraceStep step) {
        update_aux_laststep(targetObject, step.getName());
        update_aux_trace(targetObject,step) ;
    }

    /**
     * Execute relevant injectors at the beginning of a test (a  test is a sequence
     * of test-steps), which is right after the creation of the target object.
     */
    static void injectAtTestStart(Object targetObject, MkValStep step) {
        update_aux_laststep(targetObject, targetObject.getClass().getName());
        update_aux_trace(targetObject,step) ;
    }

    /**
     * Execute relevant injectors at the end of a test (which is a sequence of
     * test-steps).
     */
    static void injectAtTestEnd(Object targetObject) {
    }
}
