/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;

import Sequenic.T2.Pool;
import Sequenic.T2.Obj.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;


/**
 * An object of this class is a meta representation of a test-step
 * that, when executed, produces a value or an object.
 */
public abstract class MkValStep extends METARUN {

	private static final long serialVersionUID = 1L;
	
    /**
     * Execute the MkVal step to construct an object.
     */
    abstract public Object exec(Class CUT, Pool pool) throws InvocationTargetException ;


    /**
     * As exec, but especially for constructing a target object. It will
     * also execute slot injectors, and check the given classinvariant,
     * and produce report.
     */
    public ExecResult MkTargetObject(Class CUT, 
            Pool pool, 
            List<Method> classinvs,
            ReportersPool reporters) 
    {
        assert (this instanceof CREATE_OBJECT) ;
        
        ExecResult result =((CREATE_OBJECT) this) .MkTargetObject(CUT, pool, classinvs, reporters) ;
        
        return result ;
        
    }
     
    /**
     * As the other MkTargetObject, but this does not produce a report.
     */
    public ExecResult MkTargetObject(Class CUT, 
            Pool pool, 
            List<Method> classinvs) {
        return MkTargetObject(CUT,pool,classinvs,ReportersPool.NULLreporter) ;
    }
    
    /**
     * Execute an array of MkValSteps. This is typically used to construct
     * the arguments of a method.
     */
    public static Object[] MkArguments(Class CUT, Pool pool, MkValStep[] steps) 
        throws 
        InvocationTargetException  {

        Object[] result = null;
        if (steps == null) {
            result = new Object[0];
        } else {
            result = new Object[steps.length];
            for (int i = 0; i < steps.length; i++) {
                result[i] = steps[i].exec(CUT,pool); 
            }
        }
        return result;
    }


    /**
     * Code.
     */
    public static final String ARRAY = "ARRAY";
    //public static final String FAIL_TO_CREATE_ARG = "xx NO-VAL." ;
}
