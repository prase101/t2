package Sequenic.T2;

import java.util.*;
import java.io.*;
import java.lang.reflect.*;
import Sequenic.T2.Msg.*;

/**
 * Provide utilities to help testing T2 itself.
 * 
 * @author wishnu
 */
public class Debug {

    /**
     * This is specific for testing T2.RndEngine. It checks if the Trace-file 
     * belonging to the class C contains at least one trace.
     */
    static public void assertTrfNonEmpty(Class C) {
        String fn = C.getName() + ".tr";
        TrFile trf = null;
        try {
            trf = TrFile.load(fn);
        } catch (Exception e) {
            System.out.println("## FAIL to load " + fn + ".");
            assert false;
        }
        ;
        assert !trf.traces.isEmpty();
    }

    /**
     * This is specific for testing T2.RndEngine. It checks if C's Trace-file 
     * does not exist.
     */
    static public void assertTrfEmpty(Class C) {
        File fn = new File(C.getName() + ".tr");
        assert !fn.exists();
    }

    
    /**
     * This is specific for testing T2.RndEngine. It extracts the middle part
     * of T2 report.
     */
    static public String getT2ReportContent(String fn) {
        String content = "";
        try {
            BufferedReader buf = new BufferedReader(new FileReader(fn));
            String line = buf.readLine();
            // skip initial part:
            while (line != null && !line.startsWith("** BEGIN TEST SET")) {
                line = buf.readLine();
            //if (line != null) line = buf.readLine() ;
            // extract the middle part:
            }
            while (line != null && !line.startsWith("**")) {
                content = content.concat(line);
                line = buf.readLine();
            }
            if (line != null) {
                content = content.concat(line);
            }
            buf.close();
        } catch (Exception e) {
            return null;
        }
        return content;
    }
    
    
    static public String getT2ReportWholeContent(String fn) {
        String content = "";
        try {
            BufferedReader buf = new BufferedReader(new FileReader(fn));
            String line = buf.readLine();
            while (line != null) { 
                content = content.concat(line + "\n");
                line = buf.readLine();
            }
            buf.close();
        } catch (Exception e) {
            return null;
        }
        return content;
    }
    
    static public OutputStream out = new ByteArrayOutputStream() ;
    static public PrintStream pout =  new PrintStream(out) ;
    
    static public void FlushDebugOutStream() { 
        try { out.flush() ; }
        catch (Exception e) { }
    } 
    
}
