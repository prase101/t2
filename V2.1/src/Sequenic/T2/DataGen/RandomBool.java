package Sequenic.T2.DataGen;

/**
 * An abstract class for randomly generating boolean values. A concrete 
 * implementation may implement a certain distribution.
 */
abstract public class RandomBool {
    
    public Double low  ;
    public Double high ;
    
    /**
     * Return a random boolean. It works by generating some float x according
     * to a certain distribution. A true is returned iff x in [low..high).
     * A null for low is interpreted as -inf, and likewise +inf for high. 
     */
    abstract public boolean nextValue() ;
      
}
