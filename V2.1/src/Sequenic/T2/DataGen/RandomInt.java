package Sequenic.T2.DataGen;
import org.uncommons.maths.* ;


/**
 * An asbtract class for generating a random integer between a given
 * interval [low..high). Note that it is *exclusive* high. An implementation
 * should implement the exact distribution.
 */
abstract public class RandomInt implements NumberGenerator<Integer> {
    
    abstract public Integer nextValue() ;
    abstract public int getLow() ;
    abstract public int getHigh() ; 

}
