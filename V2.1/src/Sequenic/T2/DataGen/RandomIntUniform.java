package Sequenic.T2.DataGen;

import org.uncommons.maths.*;
import org.uncommons.maths.random.*;
import java.util.* ;

/**
 * This generates a random integer, uniformly distributed between a given
 * [low..high). Note that it is *exclusive* high.
 */
public class RandomIntUniform extends RandomInt {

    private int low = 0;
    private int high = 0;
    private NumberGenerator<Double> generator;

    public RandomIntUniform(int l, int h) {
        low = l;
        high = h;
        try {
            generator = new ContinuousUniformGenerator(l, h, new Random()) ;
            // Turning off MersenneTwister due to issue with linux
            // generator = new ContinuousUniformGenerator(l, h, new MersenneTwisterRNG(new SecureRandomSeedGenerator()));
        } catch (Exception e) {
            assert false ;
            //generator = new ContinuousUniformGenerator(l, h, new MersenneTwisterRNG());
        }
    }

    public Integer nextValue() {
        if (high <= low) {
            return low;
        } else {
            return (int) Math.floor(generator.nextValue());
        }
    }

    public int getLow() {
        return low;
    }

    public int getHigh() {
        return high;
    }
}
