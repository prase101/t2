package Sequenic.T2.Engines;

import java.math.BigInteger;

/**
 * This class is used to generate all permutative combinations of r elements
 * out of a set V of n distinct elements.
 */
public class OrderedCombinationGenerator extends AbstractCombinationGenerator {
    
    private CombinationGenerator combGen ;
    private PermutationGenerator permGen ;
    private int N ;
    private int R ;
    
    public OrderedCombinationGenerator(int n, int r) {
        assert n>0 && r>0 && r<=n ;
        N = n ; R = r ;
        combGen = new CombinationGenerator(n,r) ;
        permGen = new PermutationGenerator(r) ;
        currentS = combGen.getNext() ;
    }
    
    public boolean hasMore() {
        return combGen.hasMore() || permGen.hasMore() ;
    }
    
    private int[] currentS ;    

    /**
     * Generate next combination.
     */
    public int[] getNext() {
        assert hasMore() ;
        int[] result = new int[R] ;
        if (! permGen.hasMore()) { 
            // permutation is exhausted, however the pre-condition above enforces
            // that there is at least one combination left, so get the next
            // combination and reset the permutation:
            currentS = combGen.getNext() ;
            permGen.reset();
        }
        int[] perm = permGen.getNext() ;
        for (int i=0; i<R; i++) result[i] = currentS[perm[i]] ;
        return result ;
    }

    public void reset() {
        combGen.reset() ;
        permGen.reset();
        currentS = combGen.getNext() ;
    }

    public BigInteger getNumLeft() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BigInteger getTotal() {        
        return combGen.getTotal().multiply(permGen.getTotal()) ;
    }
    
}
