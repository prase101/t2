/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T2.Engines;
import Sequenic.T2.Seq.* ;

/**
 * An abstract class representing a plugable sequence generator for 
 * {@link BaseEngine}. Such a generator offers two primitive methods, one to 
 * generate the initial meta-step (for creating the target object that
 * starts a test sequence), and one to generate a test step. It has a 
 * link to the {@link BaseEngine} it is attached to, so it can access the 
 * engine's setup and runtime state.
 * 
 * <p>The engine basically just repeatedly calls those two primitives to
 * construct the sequences. The engine controls e.g. how many sequences
 * to generate, and their lengths. The generator controls the exact steps
 * to generate, thus ultimately the traces. A default implementation is 
 * provided as an 
 * {@link BaseEngine.RandomSequenceGenerator internal class} of 
 * {@link BaseEngine}. This default implementation generates the steps randomly.
 * 
 * 
 */
abstract public class BaseEngineSeqGenerator {
    
    /**
     * A reference to the engine using this sequence generator, so that it
     * is possible for the generator to read the engine's properties and
     * state.
     */
     public BaseEngine engine ;
     
     /**
      * To generate the (meta) step that would create a target object. This
      * step is used to begin a test sequence. It returns a null if it can't
      * come up with a step.
      */     
     abstract public MkValStep META_mkTargetObject(Class C) ;

     /**
      * To generate a (meta) test step. Return null if it can't come up with a
      * step.
      */
     abstract public TraceStep META_mkTestStep(Sequenic.T2.Seq.Trace sigma) ;

     /**
      * This notifies the generator that the given sigma fails due to violation
      * to a pre-condition or other assumptions. The generator may decide to
      * redo the sequence, but with some slight variation. The default implementation
      * is to do nothing.
      */
     public void notify_asmfailing_trace(Sequenic.T2.Seq.Trace sigma) { }
     
     /**
      * This notify that a full trace has been generated that does not violate
      * any assumption. The default implementation does nothing.
      */
     public void notify_non_asmfailing_trace() { }
         
     /**
      * A method to innitialize the generator.
      */
     public void init() { }
     
     /**
      * Return true if the generator is exhausted (it can't generate no further
      * step).
      */
     abstract public boolean isExhausted() ;
         
}
