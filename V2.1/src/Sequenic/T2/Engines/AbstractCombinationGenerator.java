package Sequenic.T2.Engines;
import java.math.BigInteger;

/**
 * Just defining a common interface for Michael Gilleland's combination generators.
 */
abstract public class AbstractCombinationGenerator {
    
    /**
     * To reset the generator.
     */
    abstract public void reset() ;
    
    /**
     * Return the number of combinations that are left (still to generate).
     */
    abstract public BigInteger getNumLeft() ;
    
    /**
     * Return total number of permutations
     */
    abstract public BigInteger getTotal() ;
    
    /**
     * Return true if there is still a combination to generate.
     */
    abstract public boolean hasMore() ;

    /**
     * Generate next combination.
     */
    abstract public int[] getNext() ;
    
}
