package Sequenic.T2 ;
import java.io.* ;
import java.util.* ;

/**
 * A base domain provides a supply of objects. When T2's {@link
 * Sequenic.T2.RndEngine test engine} needs to generate an object of a
 * class C, it first checks if it can get it from the base domain. If
 * it can get one, say x, then the engine will create a <i>clone</i>
 * of x and use the clone. If the base domain cannot supply an
 * instance of C, then the engine continues with either looking an
 * instance in its own {@link Sequenic.T2.Pool object pool}, or creates a
 * fresh instance. See also the global explanation in {@link Sequenic.T2
 * Sequenic.T2 package overview}.
 * 
 * <p>For cloning the engine relies on serialization, so it is
 * essential that objects supplied by a base domain are
 * serializable. Cloning is done to make sure that the objects in the
 * base domain are shielded from the engine's side effect. The cloning
 * is btw not done by the base domain itself; it is the responsibility
 * of the test engine. So if you implement your own test engine, do
 * keep this in mind.
 *
 * <p>Base domain essentially defines the value space over which the
 * test engine will range. E.g. we can make it very small, large but
 * still finite, or unbounded.
 *
 * <p>To define his own base domain, the test engineer should provide
 * an implementation of this abstract class, and pass it to the test
 * engine, e.g. via the {@link Sequenic.T2.RndEngine#RndTest RndTest API}. A
 * {@link Sequenic.T2.BaseDomainSmall default implementation} is provided.
 *
 */
public abstract class BaseDomain {

	/**
	 * Draw a random object of class C from this base domain. If none
	 * can be found, it returns null. If an object can be found, it
	 * will be returned in return[0]. It is essential that the method
	 * only draws an object of exactly the class C. So it won't search
	 * in the subclasses.
	 */
	abstract public Object[] get(Class C) ;

}