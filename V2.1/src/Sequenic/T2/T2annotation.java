package Sequenic.T2;

import java.lang.annotation.*; 

/**
 * Define some annotations for T2.
 */
public class T2annotation {

	/**
	 * Annotation to mark that a member of CUT should be excluded 
	 * from T2 testing.
	 */ 
	@Retention(RetentionPolicy.RUNTIME) 
	public @interface exclude{ }
	
	@Retention(RetentionPolicy.RUNTIME) 
	public @interface option{
		String value() ;
	}
	
	
}
