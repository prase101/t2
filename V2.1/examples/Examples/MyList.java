package Examples ;
import java.io.* ;
import java.util.* ;

/**
 * A simplistic implementation of linked-list. This also demonstrates
 * how T2 deals with type variable (generics).
 */
public class MyList<T>  {

	public Link list ;

	private class Link {

		T item ;
		Link next ;

		// constructors:
		Link(T i) { item = i ; next = null ; }
		Link(T i, Link p) { item=i; next=p; }

		String show() {
			String r = ";" + item ;
			if (next == null) r = r + " END" ;
			else r = r + next.show() ;
			return r ;
		}

		void print() { System.out.println("> " + this.show()) ; }
	}


	/**
	 * The class invariant requiring that the internal list maintained
	 * by this class is not circular.
	 */
	private boolean classinv() {
        List<Link> visited = new LinkedList<Link>() ;
		if (list == null) return true ;
		Link p = list ;
        visited.add(p) ;
        boolean circular = false ;
		while (p.next !=null && !circular) {
            p = p.next ;
            circular = visited.contains(p) ;
        }
		return !circular ;
	}

	/**
	 * A method for inserting an element at the head of the list.
	 */ 
	public void insert(T i) {
		Link u = new Link(i) ;
		u.next = list ;
		list = u ;
	}

	/**
	 * A method for deleting all occurences of x from the list. This
	 * method has an error: it will miss removing x if it
	 * occurs as the first element in the list. Another error: when 
	 * null is passed as x.
	 */
	public void remove(T x) {
		if (list==null) return ;
		Link p = list ;
		while (p!=null) { 
			if (p.next!=null && x == p.next.item) p.next = p.next.next ;
            p = p.next ;
		} 
	}

	/**
	 * The specification of remove; it requires that after the removal
	 * x does not appear in the list.
	 */
	public void remove_spec(T x) {
		remove(x) ;
		Link p = list ;
		boolean ok = true ;
		while (p != null && ok) { ok = p.item != x ; p=p.next ; }
        assert ok : "POST" ;
	}

    static public void main(String[] args) {
        // Calling T2 from here:
        Sequenic.T2.Main.main(MyList.class.getName() + " --exclfield");
    }
    
    
}