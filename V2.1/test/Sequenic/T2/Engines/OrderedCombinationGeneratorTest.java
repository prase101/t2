package Sequenic.T2.Engines;

import org.junit.Test;
import Sequenic.T2.Obj.Show;

public class OrderedCombinationGeneratorTest {

    static void printComb(int[] a) {
        System.out.print(">>> ");
        for (int i = 0; i < a.length; i++) {
            System.out.print("," + a[i]);
        }
        System.out.println("");
    }

    @Test
    public void test0() {
        OrderedCombinationGenerator gen = new OrderedCombinationGenerator(3,2);
        System.out.println("comb(3,2) = " + gen.getTotal());
        int N = gen.getTotal().intValue() ;
        assert N==6 ;
        int[] comb ;
        for (int i=0 ; i<N; i++ ) {
            comb = gen.getNext() ;
            printComb(comb) ;
        }
    }
    
    @Test
    public void test1() {
        OrderedCombinationGenerator gen = new OrderedCombinationGenerator(3,3);
        System.out.println("comb(3,3) = " + gen.getTotal());
        int N = gen.getTotal().intValue() ;
        assert N==6 ;
        int[] comb ;
        for (int i=0 ; i<N; i++ ) {
            comb = gen.getNext() ;
            printComb(comb) ;
        }
    }
    
    @Test
    public void test2() {
        OrderedCombinationGenerator gen = new OrderedCombinationGenerator(3,1);
        System.out.println("comb(3,1) = " + gen.getTotal());
        int N = gen.getTotal().intValue() ;
        assert N==3 ;
        int[] comb ;
        for (int i=0 ; i<N; i++ ) {
            comb = gen.getNext() ;
            printComb(comb) ;
        }
    }
}