package Sequenic.T2.Engines;
import Sequenic.T2.Debug ;
import java.io.* ;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author underdarkprime
 */
public class ReplayTest {

    public ReplayTest() {
    }

    
    
    static public class C1 {

        private int x = 0;
        public C1 next = null;

        public C1(int x0) {
            x = x0;
        }

        public void m() {
        }

        public void ouch() {
            assert false;
        }
    }

    @Test
    public void test1() {
        System.out.println("@@@ 3x violations and 1x non violation...");
        BaseEngine engine = new BaseEngine(C1.class);
        engine.maxNumViolations = 3;
        engine.saveThisMany = 1;
        
        Debug.FlushDebugOutStream() ;
        engine.out = Debug.pout ;
        engine.reporters.setOutStream(Debug.pout) ;
        engine.timedRun() ;
        assert Debug.out.toString().contains("Saving 4 traces");
        
        
        Replay replayer = new Replay(C1.class.getName() + ".tr");
        Debug.FlushDebugOutStream() ;
        replayer.out = Debug.pout ;
        replayer.reporters.setOutStream(Debug.pout);
        //replayer.replayUpToThisMany = 4 ;
        try { replayer.load(); }
        catch (Exception e) { e.printStackTrace(); assert false ; }
        //System.out.println(">>>" + replayer.loadedTraces.traces.size()) ;
        replayer.selectTraces();
        //System.out.println(">>>" + replayer.selected.traces.size()) ;
        replayer.timedRun() ;
        assert Debug.out.toString().contains("3 assertion violations");
    }
    
    /*
    public static void main(String[] args){ 
        ReplayTest test = new ReplayTest();
        test.test1() ; 
    }
    */
    
}