package Sequenic.T2.Engines;

import java.math.BigInteger;
import Sequenic.T2.Obj.Show ;
import org.junit.Test;

/**
 *
 * @author underdarkprime
 */
public class CombinationGeneratorTest {

    static void printComb(int[] a) {
        System.out.print(">>> ") ;
        for (int i=0; i<a.length; i++) System.out.print("," + a[i]) ;
        System.out.println("") ;
    }
    
    
    @Test
    public void test0() {
        CombinationGenerator gen = new CombinationGenerator(3,2) ;
        System.out.println(">>>" + gen.getTotal()) ;
        System.out.println(">>>" + Show.show(gen.getNext())) ;
        System.out.println(">>>" + Show.show(gen.getNext())) ;
        System.out.println(">>>" + Show.show(gen.getNext())) ;
    }

}