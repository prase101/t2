package Sequenic.T2;

import org.junit.Test;

public class Main_Test_8 {

    static public class Cprimtypes { 

		public void mbool (boolean x) { } 
		public void mBool (Boolean x) { }
		
		public void mbyte (byte x) { } 
		public void mByte (Byte x) { }
		
		public void mshort (short x) { } 
		public void mShort (Short x) { }
		
		public void mint (int x) { } 
		public void mInt (Integer x) { }
		
		public void mlong (long x) { } 
		public void mLong (Long x) { }

		public void mfloat (float x) { } 
		public void mFloat (Float x) { }
		
		public void mdoub (double x) { } 
		public void mDoub (Double x) { }

		public void mchar (char x) { } 
		public void mChar (Character x) { }

	}
	
	@Test
	public void test_primtypes() {
		System.out.println("@@@ check if T2 generates all sorts of primitive types and their boxings ...");
        Class C = Cprimtypes.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --meth=mbool --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mBool --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mbyte --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mByte --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mshort --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mShort --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mint --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mInt --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mlong --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mLong --nullprob=0.5 -debug ");
        Main.main(C.getName() + " --meth=mfloat --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mFloat --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mdouble --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mDouble --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mchar --nullprob=0.5 --debug ");
        Main.main(C.getName() + " --meth=mCharacter --nullprob=0.5 --debug ");
	}

	
}
