package Sequenic.T2;

import Sequenic.T2.*;
import java.io.*;
import java.util.*;

import org.junit.Test;

public class Main_Test_3 {

    /**
     * Test purpose: check if T2 can generate array.
     */
    static public class Carray{
        boolean[] sizes = {false,false,false,false,false} ;
        public void m(Object[] a) {
            assert a!=null : "PRE" ;
            sizes[a.length % 5] = true ;
        }
        public void check() {
            boolean allDone = sizes[0] && sizes[1]  && sizes[2] && sizes[3] && sizes[4] ;
            assert !allDone ;
        }
    }
 
    @Test
    public void test_array() {
        System.out.println("@@@. check if T2 can arrays of various size...");
        Class C = Carray.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --silent --exclfield --nmax=20000 --lenexec=10");
        Debug.assertTrfNonEmpty(C);
    }
    
    /**
     * Test purpose: check if T2 can generate collection.
     */
    static public class Ccollection{
        boolean[] sizes = {false,false,false,false,false} ;
        public void m(List a) {
            assert a!=null : "PRE" ;
            sizes[a.size() % 5] = true ;
        }
        public void check() {
            boolean allDone = sizes[0] && sizes[1]  && sizes[2] && sizes[3] && sizes[4] ;
            assert !allDone ;
        }
    }
 
    @Test
    public void test_collection() {
        System.out.println("@@@. check if T2 can collection of various size...");
        Class C = Ccollection.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --silent --exclfield --nmax=20000 --lenexec=10");
        Debug.assertTrfNonEmpty(C);
    }
            
    /**
     * Test purpose: check if T2 can generate null, and can exclude null.
     */
    static public class Cnull {

        void m1(Cnull x) {
            assert x == null;
        }

        void m2(Cnull x) {
            assert x != null;
        }
    }

    @Test
    public void test_Cnull() {
        System.out.println("@@@. check if T2 can generate null, and can exclude null...");
        Class C = Cnull.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m2 --nullprob=0 --silent");
        Debug.assertTrfEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --nullprob=1 --silent");
        Debug.assertTrfNonEmpty(C);
    }

    /**
     * Test purpose: check if elemty option is handled correctly.
     */
    static public class Celemty1<T> {
    
        static Class OBJECT = Sequenic.T2.OBJECT.class ;
        
        void m0(Object x) { assert x.getClass() == OBJECT ; }

        void m1(T x) { assert x.getClass() == OBJECT ; }

        <U> void m2(U x) { assert x.getClass() == OBJECT ; }

        void m3(List<T> s) {
            if (!s.isEmpty()) {
                T x = s.get(0);
                assert x.getClass() == OBJECT ;
            }
        }

        void m4(T[] s) {
            if (s.length > 0) {
                T x = s[0];
                assert x.getClass() == OBJECT ;
            }
        }
    }

    static public class Celemty2<T> {

        void m10(Object x) { assert x instanceof Integer; }

        void m11(T x) { assert x instanceof Integer; }

        <U> void m12(U x) {assert x instanceof Integer; }

        void m3(List<T> s) {
            if (!s.isEmpty()) {
                T x = s.get(0);
                assert x instanceof Integer;
            }
        }

        void m4(T[] s) {
            if (s.length > 0) {
                T x = s[0];
                assert x instanceof Integer;
            }
        }
    }

    @Test
    public void test_elemty() {
        System.out.println("@@@. check if elemty option is handled correctly...");
        Class C = Celemty1.class;
        // with no elemty, Object and tyvars should be replaced by Sequenic.T2.OBJECT:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --nullprob=-1 ");
        Debug.assertTrfEmpty(C);

        C = Celemty2.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --nullprob=-1 --elemty=java.lang.Integer --silent");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: check if depthobj option is handled correctly.
     */
    static public class Cdepthobj<T> {

        private Cdepthobj next = null;

        public Cdepthobj(Cdepthobj n) {
            next = n;
        }

        private int length() {
            if (next == null) {
                return 1;
            } else {
                return 1 + next.length();
            }
        }

        void m0() {
            //System.out.println(">>>" + length()) ;
            assert length() <= 10;
        }

        void m1() {
            //System.out.println(">>>" + length()) ;
            assert length() < 4;
        }

        void m2() {
            //System.out.println(">>>" + length()) ;
            assert length() != 1;
        }
    }

    @Test
    public void test_depthobj() {
        System.out.println("@@@. check if depthobj option is handled correctly...");
        Class C = Cdepthobj.class;
        // created objects will have the actual depth <= specified
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --depthobj=10 --silent");
        Debug.assertTrfEmpty(C);

        // check that depth 1 and depth max are also generated:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --depthobj=4 --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m2 --depthobj=4 --silent");
        Debug.assertTrfNonEmpty(C);
    }

    /**
     * Test purpose: check if T2 can pick and ignore objects from the pool.
     */
    static public class Cpickpool0 {

        private static int lastId = 0;
        private int id;

        int getId() {
            return id;
        }

        public Cpickpool0() {
            id = lastId;
            lastId++;
        }

        boolean isNewestObject() {
            return id + 1 == lastId;
        }

        void m0(Cpickpool0 y) {
            assert y != null : "PRE";
            //System.out.println(">>>" + y.getId() + " | " + lastId) ;
            assert y.isNewestObject();
        }

        void m1(Cpickpool0 y) {
            assert y != null : "PRE";
            assert !y.isNewestObject();
        }

        void m2(Cpickpool0 y) {
            assert y != null : "PRE";
            assert y == this;
        }
    }

    static public class Cpickpool1 extends Cpickpool0 {

        void m3(Cpickpool0 y) {
            assert !(y instanceof Cpickpool1);
        }
    }

    @Test
    public void test_pickpool() {
        System.out.println("@@@ check if T2 can pick and ignore objects from the pool...");
        Class C = Cpickpool0.class;
        System.out.println("@   check if T2 can get an object from pool") ;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --silent");
        Debug.assertTrfNonEmpty(C);

        System.out.println("@   check if T2 can generate fresh object") ;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --silent");
        Debug.assertTrfNonEmpty(C);

        System.out.println("@   check if T2 can force to use only pool") ;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m2 --pickpoolprob=1 ");
        Debug.assertTrfEmpty(C);

        System.out.println("@   check if T2 can force avoiding pool") ;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --pickpoolprob=-1 --tobjasparam=-1 --silent");
        Debug.assertTrfEmpty(C);

        System.out.println("@   check if we can get a subclass from the pool") ;
        C = Cpickpool1.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m3  --silent");
        Debug.assertTrfNonEmpty(C);
    }

    /**
     * Test purpose: to check if the interface map is working properly.
     */
    static public class Cimap {

        void m0(Comparable x) {
            assert x instanceof Integer;
        }

        void m1(Collection U) {
            assert U instanceof LinkedList;
        }

        void m2(List U) {
            assert U instanceof LinkedList;
        }

        void m3(Queue U) {
            assert U instanceof LinkedList;
        }

        void m4(Set U) {
            assert U instanceof HashSet;
        }

        void m4(Map M) {
            assert M instanceof HashMap;
        }
    }

    @Test
    public void test_imap() {
        System.out.println("@@@. check if the interface map is working properly...");
        Class C = Cimap.class;
        //check if T2 can pass tobj in parameter:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --nullprob=-1 ");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: to check if the base domain is working properly.
     */
    static public class Cbdomain {

        static BaseDomainSmall bd = new BaseDomainSmall();

        void m0(String x) {
            boolean found = false;
            String[] ss = bd.getStringsupply();
            for (int i = 0; i < ss.length && !found; i++) {
                found = ss[i].equals(x);
            }
            assert found;
        }

        void m1(char x) {
            char[] ss = bd.getCharsupply();
            boolean found = false;
            for (int i = 0; i < ss.length && !found; i++) {
                found = ss[i] == x;
            }
            assert found;
        }

        void m2(int i) {
            assert -3 <= i && i < bd.getRange() - 3;
        }

        void m3(byte i) {
            assert -3 <= i && i < bd.getRange() - 3;
        }

        void m4(long i) {
            assert -3 <= i && i < bd.getRange() - 3;
        }

        void m5(Float i) {
            assert -3 <= i && i < bd.getRange() - 3;
        }
    }

    @Test
    public void test_bdomain() {
        System.out.println("@@@. check if the base domain is working properly...");
        Class C = Cbdomain.class;
        //check if T2 can pass tobj in parameter:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --silent");
        Debug.assertTrfEmpty(C);
    }
    
    /**
     * Test purpose: to check if using a custom basedomain we can generate
     * a null Integer.
     */
    static public class classThatExpectsNullInteger {
    	
    	public void m1(int x) { }
    	
    	public void checknull(Integer x) {
    		assert x!= null ;
    	}
    }
    
    static public class MyBaseDomain extends BaseDomain0 {
    	
    	public MyBaseDomain() {
    		super() ;
    		ArrayList<Serializable> intdomain = new ArrayList<Serializable>() ;
    		intdomain.add(new Integer(0)) ;
    		intdomain.add(new Integer(1)) ;
    		
    		ArrayList<Serializable> INTdomain = new ArrayList<Serializable>() ;
    		// Hmm ... do I need to cast null?
    		INTdomain.add((Integer) null) ;
    		INTdomain.add(new Integer(1)) ;
    		// Replacing original domains for int and Integer with the above:
    		domain.put(Integer.TYPE,intdomain) ;
    		domain.put(Integer.class,INTdomain) ;
    	}
    	
	}
    	
    @Test
    public void test_custom_bdomain_null_Integer() {
        System.out.println("@@@. check if a custom basedomain can generate null Integer...");
        Class C = MyBaseDomain.class;
        //check if T2 can pass tobj in parameter:
        TrFile.delete(C);
        String custombdom = MyBaseDomain.class.getName() ;
        Main.main(C.getName() + " --debug --silent --bdomain=" + custombdom);
        Debug.assertTrfNonEmpty(C);
    }	
    	
    /**
     * Test purpose: to check if the tobjasparam option is handled correctly.
     */
    static public class Ctobjasparam {

        void m0(Ctobjasparam x) {
            assert x != this;
        }
        static private Ctobjasparam tobj = null;

        void m1(Ctobjasparam x) {
            assert x == this : "PRE";
            tobj = x;
        }

        static void s1(Ctobjasparam x) {
            assert tobj != null : "PRE";
            assert x != tobj;
        }
        static private Ctobjasparam last = null;

        static void s2(Ctobjasparam x) {
            last = x;
        }

        void m2() {
            assert last != this;
        }
    }

    @Test
    public void test_tobjasparam() {
        System.out.println("@@@. check if the tobjasparam option is handled correctly...");
        Class C = Ctobjasparam.class;
        //check if T2 can pass tobj in parameter:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --silent");
        Debug.assertTrfNonEmpty(C);

        //check if T2 can pass tobj in parameter of a static method:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --meth=s1 --silent");
        Debug.assertTrfNonEmpty(C);

        // check if T2 can surpress passing tobj as parameter
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --meth=m2 --meth=s2 --tobjasparam=-1 --pickpoolprob=-1 --silent");
        Debug.assertTrfEmpty(C);

    }
}
