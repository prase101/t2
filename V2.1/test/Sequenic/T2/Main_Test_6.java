package Sequenic.T2;

import Sequenic.T2.*;
import org.junit.Test;

public class Main_Test_6 {

    static public class CsearchMode {

        private int status = 0 ;
        
        public CsearchMode() {}

        public void m1() { 
            assert status==0 : "PRE" ;
            status=1 ;
        }
        
        public void m2(){
            assert status==1 : "PRE" ;
            status=2 ;
        }
        
        public void m3(){
            assert status==2 : "PRE" ;
            status=3 ;            
        }

        public void m4(){
            assert status==3 : "PRE" ;
            status=4 ;            
        }
        
        public void m5(){
            assert status==4 : "PRE" ;
            assert false ;      
        }
    }
    
    /**
     * Test purpose: check if the search-mode is working.
     */
    @Test
    public void test_regress() {
        System.out.println("@@@ Check if the search-mode is working...");
        Class C = CsearchMode.class;
        
        // This may find the violation, but the chance is small:
        TrFile.delete(C);
        Main.main(C.getName() + " --lenexec=5") ;
        Debug.assertTrfEmpty(C);
        
        // While this one will have high probability to find the error:
        TrFile.delete(C);
        Main.main(CsearchMode.class.getName() + " --lenexec=6 --searchmode=100") ;
        Debug.assertTrfNonEmpty(C);
    }
    
}
