package Sequenic.T2;
import java.io.*; 
import org.junit.Test;

public class Main_combinatoric_engine_test_1 {

    static public class C1 {
        private int[] order = {-1,-1,-1} ;
        private int k = 0 ;
        void m1() { order[k]=1 ;k++ ; }
        void m2() { order[k]=2 ;k++ ; }
        void m3() { order[k]=3 ;k++ ; }
        void m4() { order[k]=4 ;k++ ; }
        void ouch() { 
            order[k]=5 ;
            // Check if we can reach this combination:
            assert !(order[0]==3 && order[1]==1 && order[2]==5) ;
        }
    }

    @Test
    public void test_particular_combination() {
        System.out.println("@@@ check if we can reach a particular combination...");
        Class C = C1.class;
        TrFile.delete(C);
        Main.main("-C " + C.getName() + " --debug --lenexec=4 --silent");
        Debug.assertTrfNonEmpty(C);
    }
    
    static public class C2{
        int x = 0 ;
        void m1() { }
        void m2() { }
        void ouch() { assert x==0 ; }
        }
    
    @Test
    public void test_can_update_field() {
        System.out.println("@@@ check if field updates are also generated...");
        Class C = C2.class;
        TrFile.delete(C);
        Main.main("-C " + C.getName() + " --debug --lenexec=3 --silent");
        Debug.assertTrfNonEmpty(C);
    }
    
    static public class C3{
        void m1() { }
    }
    
    @Test
    public void test_duplicates() {
        System.out.println("@@@ check if duplicates are generated...");
        Class C = C3.class;
        TrFile.delete(C);
        File log1 = new File("test1.log") ;
        log1.delete() ;
        // Main.main("-C " + C.getName() + " --debug --lenexec=2 --silent ");
        Main.main("-C " + C.getName() + " --debug --lenexec=2 --silent --outfile=test1.log");
        String report = Debug.getT2ReportWholeContent("test1.log");
        //System.out.print(report) ;
        assert report.contains("total number of traces : 2");
    }
    
    
}
