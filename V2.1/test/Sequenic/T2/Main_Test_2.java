package Sequenic.T2;

import Sequenic.T2.*;
import java.io.*;
import java.lang.*;
import org.junit.Test;
//import static org.junit.Assert.*;

public class Main_Test_2 {

    /**
     * Test purpose: check if T2 can reach overloaded methods.
     */
    static public class Coverload {
        
        boolean m1called = false ;
        boolean m2called = false ;
        public void m() { m1called = true ; }
        public void m(int x) { m2called = true ; }
        public void check() { 
            // will throw a violation only when both versions of m
            // have been called:
            assert !(m1called && m2called) ; }
    }
    
    @Test
    public void test_overload() {
        System.out.println("@@@ check if T2 can reach overloaded methods...");
        Class C = Coverload.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --silent"); 
        Debug.assertTrfNonEmpty(C);
    }
    
    /**
     * Test purpose: check if T2 can reach superclass' classinv.
     */
    static public class CsuperClassInv0 {

        private boolean classinv() {
            return false;
        }
    }

    static public class CsuperClassInv1 extends CsuperClassInv0 {

        private boolean classinv() {
            return true;
        }

        void m() {
        }
    }

    @Test
    public void test_superClassInv() {
        System.out.println("@@@ check if T2 can reach superclass' classinv...");
        Class C = CsuperClassInv1.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --silent");
        Debug.assertTrfEmpty(C);
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --inclsuperinv --silent");
        Debug.assertTrfNonEmpty(C);
    }

    /**
     * Test purpose: check if T2 can reach and exclude superclass' methods.
     */
    static public class CsuperMeth0 {

        private void m0() {
            assert false;
        }

        void m1() {
            assert false;
        }

        protected void m2() {
            assert false;
        }

        public void m3() {
            assert false;
        }

        static public void s(CsuperMeth0 x) {
            assert false;
        }
    }

    static public class CsuperMeth1 extends CsuperMeth0 {

        void mm() {
        }
    }

    @Test
    public void test_superMeth() {
        System.out.println("@@@ check if T2 can reach and exclude superclass' methods...");
        Class C = CsuperMeth1.class;

        // m0 is private of superclass, so it should not be in the scope:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m0 --inclprivate --silent");
        Debug.assertTrfEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m1 --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m2 --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=m3 --silent");
        Debug.assertTrfNonEmpty(C);

        TrFile.delete(C);
        Main.main(C.getName() + " --debug --meth=s --silent");
        Debug.assertTrfNonEmpty(C);

        // now testing exclusion:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --ownclassonly --silent");
        Debug.assertTrfEmpty(C);

    }

    /**
     * Test purpose: check if T2 can reach and exclude superclass' fields.
     */
    static public class CsuperField0 {
        int x1 = 222;      
    }

    static public class CsuperField1 extends CsuperField0 {

        void check_x1() { assert x1==222 ; }

    }

    @Test
    public void test_superField() {
        System.out.println("@@@ check if T2 can reach and exclude superclass' fields...");
        Class C = CsuperField1.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug  --lenexec=10 --silent");
        Debug.assertTrfNonEmpty(C);

        //Now check for exclusion:
        C = CsuperField1.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --fupdateprob=0.99 --ownclassonly --lenexec=10 --silent");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: check if T2 handles dynamic binding of methods correctly.
     */
    static public class CdynBindMeth0 {

        private void m0() { assert false; }
        void m1() { assert false; }
        protected void m2() { assert false; }
        public void m3() { assert false; }
        static public void s(CdynBindMeth0 x) { assert false; }
    }

    static public class CdynBindMeth1 extends CdynBindMeth0 {

        private void m0() {}
        void m1() {}
        protected void m2() {}
        public void m3() {}
        static public void s(CdynBindMeth0 x) {}
    }
    
    static public class CdynBindMeth2 extends CdynBindMeth0 {

    }

    @Test
    public void test_dynBindMeth() {
        System.out.println("@@@ check if T2 handles dynamic binding of methods correctly...");
        Class C = CdynBindMeth1.class;
        // non-private methods are overriden:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --exclstatic --silent");
        Debug.assertTrfEmpty(C);
    }

    /**
     * Test purpose: check if T2 handles fields hiding correctly.
     */
    static public class CfieldHiding0 {
        private int x0 = 222;
        int x1 = 222;
        protected int x2 = 222;
        public int x3 = 222;
    }

    static public class CfieldHiding1 extends CfieldHiding0 {
        private int x0 = 333;
        int x1 = 333;
        protected int x2 = 333;
 
        private boolean classinv() {
            return super.x0 == 222 && super.x1==222 && super.x2==222 && super.x3==222 ;
        }

    }

    @Test
    public void test_fieldHiding() {
        System.out.println("@@@ check if T2 handles fields hiding correctly...");
        Class C = CfieldHiding1.class;
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --ownclassonly --fupdateprob=0.99 --silent");
        Debug.assertTrfEmpty(C);
        // However if we now extend the scope to superclass, T2 should be able to reach to 
        // super's fields (in this case x3), even if they are shadowed in CUT:
        TrFile.delete(C);
        Main.main(C.getName() + " --debug --fupdateprob=0.99 --silent");
        Debug.assertTrfNonEmpty(C);
    }
}
