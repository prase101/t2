package Examples;
import org.junit.Test;

/**
 * Demonstrating that we can easily integrate T2 into Junit framework; see how
 * automated testing with T2 can go together with hand written tests in Junit.
 */
public class MyListTest {

    @Test
    public void test1() {
        Sequenic.T2.Main.Junit(MyList.class.getName() + " --exclfield --savegoodtr=10") ;
    }
    
    @Test
    public void test2() {
        MyList s = new MyList() ;
        Object x = new Object() ;
        s.insert(x);
        s.insert(x) ;
        s.remove_spec(x) ;
    }

}