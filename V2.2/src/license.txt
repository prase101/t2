---------------------------------------------------------------------
T2 LICENSE
---------------------------------------------------------------------

This copyright and license statements apply to the entire T2 software.

Copyright 2007 Wishnu Prasetya.

T2 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) version 3, as
published by the Free Software Foundation.

T2 is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

A copy of the GNU General Public License (GPL) can be found in the
file gpl_license.txt, which is included in this software. If it is
missing, see http://www.gnu.org/licenses.

---------------------------------------------------------------------