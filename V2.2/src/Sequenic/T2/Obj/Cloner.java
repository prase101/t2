package Sequenic.T2.Obj ;
import java.io.* ;
import java.util.* ;
import java.lang.reflect.* ;
//import Sequenic.T2.examples.* ;

/** 
 *   Utility to do deep cloning of an object.
 */
public class Cloner {

	// static public int BUFSIZE = 1000 ;

	/**
	 * Return a deep-clone of an object. Currently implemented via
	 * serialization; thus assuming that the cloned object is
	 * serializable.
	 */
	static public <T> T clone(T o) 
		throws IOException, 
			   ClassNotFoundException 
	{
		ByteArrayOutputStream outstream = new ByteArrayOutputStream() ;
		ObjectOutputStream cout = new ObjectOutputStream(outstream) ;
		cout.writeObject(o) ; 
		ByteArrayInputStream instream = new ByteArrayInputStream(outstream.toByteArray()) ;
		ObjectInputStream cin = new ObjectInputStream(instream) ;
		T copy = (T) cin.readObject() ;
		return copy ;

	}

	/**
	 * Same as clone, but as an ordinary method.
	 * 
	 * @see Sequenic.T2.Obj.Cloner#clone
	 */
	public <T> T cloneIt(T o) 
			throws IOException, 
			   ClassNotFoundException 
	{	return clone(o) ; }
	
	
	/**
	 * Spec of cloneIt. It only checks at the top level of the object graphs.
	 * Primitive values of o and its clone should be equal, and pointers 
	 * should be distinct.
	 */
	public <T> T cloneIt_spec(T o) {
		
		T cloned = null ;
		
		try { 
			cloned = cloneIt(o) ;
			assert canbeInjectedTo(o,cloned, new LinkedList()) 
					&& canbeInjectedTo(cloned,o, new LinkedList())
					: "POST" ;
		}
		catch (Exception e) { } 			
		return cloned ;		
	} 
	
	/**
	 * Check if the object x can be injected to y.
	 */
	private static boolean canbeInjectedTo(Object x, Object y, Collection visited) {
		if (x == null) return y==null ;
		if (y == null) return false ;
		if (visited.contains(x)) return true ;
		Class C = x.getClass() ;
		if (!C.getName().equals(y.getClass().getName())) return false ;
		Field[] fields = C.getDeclaredFields() ;
		int i = 0 ;
		for ( ; i<fields.length ; i++) fields[i] . setAccessible(true) ;
		boolean ok = true ;	
		visited.add(x) ;
		try { 
			for (i=0 ; i<fields.length && ok ; i++)
				if (fields[i].getType().isPrimitive())
						ok = fields[i].get(x).equals(fields[i].get(y)) ; 
					else
						ok = canbeInjectedTo(fields[i].get(x),fields[i].get(y),visited) ;
		}
		catch (Exception e) { return false ; } 
		return ok ;		
	}
	
}
