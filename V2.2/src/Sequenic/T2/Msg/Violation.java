package Sequenic.T2.Msg;

/**
 * This is added mainly as a mechanism to Junit to signal that T2 finds violations.
 * This is relevant when integrating with Junit. Ideally we need to pass on
 * violations found by T2 as Junit data structures, so that tools processing
 * Junit can also process violation infos from T2. But I don't really know how
 * to hook into Junit. Future work. Anyway, just signaling works.
 */
public class Violation extends Error {
	
	public Violation() { super() ; }
	
	public Violation(Throwable e) { super(e) ; }
    
}
