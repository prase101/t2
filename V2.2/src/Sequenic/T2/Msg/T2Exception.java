package Sequenic.T2.Msg ;

public class T2Exception extends Exception {

	public T2Exception(String msg) { super(msg) ; }

	public T2Exception(String msg, Throwable cause) { 		
		super(msg, cause) ; 
	}
	
}