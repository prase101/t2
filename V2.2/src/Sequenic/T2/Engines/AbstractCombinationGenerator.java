package Sequenic.T2.Engines;
import java.math.BigInteger;

/**
 * Just defining a common interface to combinations-like generators. This
 * include permutation generators, and product generators. 
 */
abstract public class AbstractCombinationGenerator {
    
	/**
	 * This holds the current combination.
	 */
	protected int[] a;
	
	/**
	 * The number of combinations left to generate.
	 */
	protected int numLeft;

	/**
	 * The total number of combinations to generate.
	 */
	protected int total;
	
    /**
     * To reset the generator.
     */
    abstract public void reset() ;
    
    /**
     * Return the number of combinations that are left (still to generate).
     */
    public int getNumLeft() { return numLeft ; }
    
    /**
     * Return total number of permutations
     */
    public int getTotal() { return total ; }
    
    /**
     * Return true if there is still a combination to generate.
     */
    public boolean hasMore() {
    	return numLeft>0 ;
    }

    /**
     * Generate the next combination. The combination will be copied
     * to the passed array a. The array should be of the right size.
     */
    abstract public void getNext(int[] a) ;
    
    /**
     * Copy the current combination to the array res. The array should be
     * of the right size.
     */
    public void getCurrent(int[] res) {
  	  assert res.length == a.length ;
	  copyArray(a,res) ;
	  return ;
    }
    
    /**
     * Just a utility method for copying the content of the array
     * "from" to the array "to". 
     */
    protected void copyArray(int[] from, int[] to){
   	 int N = Math.min(from.length, to.length) ;
   	 for (int i=0; i<N; i++) to[i] = from[i] ;
     }
    
    
    //------------------
    // Compute factorial
    //------------------
    public static long getFactorial (int n) {
      long fact =  1 ;
      for (int i = n; i > 1; i--) { fact = fact * i ; }
      return fact;
    }
    
}
