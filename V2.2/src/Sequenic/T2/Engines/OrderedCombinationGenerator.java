package Sequenic.T2.Engines;

import java.math.BigInteger;

/**
 * This class is used to generate all permutative combinations of r elements
 * out of a set V of n distinct elements.
 */
public class OrderedCombinationGenerator extends AbstractCombinationGenerator {
    
    private CombinationGenerator combGen ;
    private PermutationGenerator permGen ;
    private int N ;
    private int R ;
    
    /**
     * To hold the current combination to permutate.
     */
    private int[] currentC ;   
    
    /**
     * Holds the current permutation on the indices of currentC.
     * Applying this permutation to currentC yields what the
     * current sequence is.
     */
    private int[] currentPerm ;
    
    
    public OrderedCombinationGenerator(int n, int r) {
        assert n>0 && r>0 && r<=n ;
        N = n ; R = r ;
        combGen = new CombinationGenerator(n,r) ;
        permGen = new PermutationGenerator(r) ;
        currentC = new int[R] ;
        currentPerm = new int[R] ;
        a = new int[R] ;
        try {
        	long nFact = getFactorial (N);
        	long Fact2 = getFactorial (N - R);
        	long total = nFact / Fact2 ;
        	if (total <= (int) Integer.MAX_VALUE) this.total = (int) total ;
        	else throw new Error("The number of combinations is too large.") ;
        }
        catch (ArithmeticException e) {
        	throw new Error("The number of combinations is too large.",e) ;
        }
        reset() ;
    }
    
    private void applyPermutation(int[] res){
    	assert res.length == R ;
    	for (int i=0; i<R; i++) res[i] = currentC[currentPerm[i]] ;
    }
    
    public boolean hasMore() {
        return combGen.hasMore() || permGen.hasMore() ;
    }
    
    /**
     * Generate next combination.
     */
    @Override
    public void getNext(int[] res) {
    	
    	assert res.length == R ;
    	assert hasMore() ;
    	
        if (numLeft == total) {
        	permGen.getNext(currentPerm) ;
        	combGen.getNext(currentC) ;
        	numLeft-- ;
        	applyPermutation(a) ;
        	copyArray(a,res) ;
        	return ;
        }
        
    	
        if (permGen.hasMore()) {
        	permGen.getNext(currentPerm) ;
        }
        else {
        	combGen.getNext(currentC) ;
        	permGen.reset() ;
        	permGen.getNext(currentPerm) ;
        }
    	applyPermutation(a) ;
       	copyArray(a,res) ;
       	numLeft-- ;
        return ;
    }

    @Override
    public void reset() {
        combGen.reset() ;
        combGen.getCurrent(currentC) ;
        permGen.reset();
        permGen.getCurrent(currentPerm) ;
        applyPermutation(a) ;
        numLeft =total ;
    }
    
   
}
