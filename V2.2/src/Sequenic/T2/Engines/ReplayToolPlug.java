package Sequenic.T2.Engines;
import Sequenic.T2.*;
import Sequenic.P2.*;
import Sequenic.T2.Msg.*;
import Sequenic.T2.DataGen.* ;
import jargs.gnu.*;
import java.util.* ;
import java.io.* ;
import java.lang.reflect.* ;

/**
 * A wrapper to plug-in {@link Replay Replay-engine} to the {@ Sequenic.T2.Main Main-tool}.
 * 
 * @author underdarkprime
 */
/**
 *
 * @author underdarkprime
 */
public class ReplayToolPlug extends ToolPlug {

    // Variables holding various options:
    private CmdLineParser.Option randomTracesO = parser.addBooleanOption("randomtr");
    private CmdLineParser.Option onlyRegressViolationsO = parser.addBooleanOption("onlyviol");
    private CmdLineParser.Option replayUpToThisManyO = parser.addIntegerOption('t',"tmax") ;
    private CmdLineParser.Option maxNumOfViolO = parser.addIntegerOption('v', "violmax");
    
    private CmdLineParser.Option timeoutO = parser.addIntegerOption("timeout");

    private CmdLineParser.Option showDepthO = parser.addIntegerOption("showdepth"); 
    private CmdLineParser.Option hideIntermediateStepsO = parser.addBooleanOption("hideintermstep") ;
    private CmdLineParser.Option outStreamO = parser.addStringOption("outfile");
    
    private CmdLineParser.Option silentO = parser.addBooleanOption("silent") ;
    
    // Variable to hold a replay-engine:
    private Replay engine ;
    private PrintStream reportStream ;
    
    public ReplayToolPlug() {
        addOption(randomTracesO,"To replay randomly selected traces.") ;
        addOption(onlyRegressViolationsO,"To replay only traces marked as violating.") ;
        addOption(replayUpToThisManyO,"Replay up-to this many.") ;
        addOption(maxNumOfViolO,"Will stop after finding this many violations.") ;
        addOption(timeoutO, "Timeout period.");
        addOption(showDepthO, "Maximum depth of objects showed in reports.");
        addOption(hideIntermediateStepsO,"To hide intermediate steps.") ;
        addOption(outStreamO, "Will print the report to the specified file rather than to the console.");
        addOption(silentO, "Will not print execution trails. Overall statistics are still shown.");
    }
    
    
    public void configure(String[] options) 
            throws 
            CmdLineParser.IllegalOptionValueException, 
            CmdLineParser.UnknownOptionException {
        
        assert options.length > 0 ;
        
        // Now parsing all options:
        parser.parse(options); 
        // Create a replay-engine with a default configuration:
        engine = new Replay(options[0]) ;
          
        // Now do the configuration work:
        int n = 0 ;
	double p = 0 ;
	String s = null ;
        
        engine.replayRandomTraces = parser.getOptionValue(randomTracesO) != null ;
        engine.replayOnlyViolatingTraces = parser.getOptionValue(onlyRegressViolationsO) != null ;
        engine.replayUpToThisMany = (Integer) parser.getOptionValue(replayUpToThisManyO,engine.replayUpToThisMany) ;
        engine.stopAfterThisManyViolations  = (Integer) parser.getOptionValue(maxNumOfViolO,engine.stopAfterThisManyViolations) ;
        engine.timeOutPeriod = (Integer) parser.getOptionValue(timeoutO,engine.timeOutPeriod) ;

        engine.maxShowDepth = (Integer) parser.getOptionValue(showDepthO,engine.maxShowDepth) ;        
        engine.printIntermediateStepsOption = parser.getOptionValue(hideIntermediateStepsO) == null ;
        
        engine.silent = parser.getOptionValue(silentO) != null;
        
        s = (String) (parser.getOptionValue(outStreamO)) ;
        if (s != null) {
            try { reportStream = new PrintStream(s) ; }
            catch (Exception e) {
                throw new T2Error("Fail to open " + s + ".", e) ;
            }
            engine.out = reportStream ;
            engine.reporters.setOutStream(reportStream) ;
        }
        
        // Now load and select the traces:
        try {
            engine.load();
            engine.selectTraces();
        }
        catch (T2Exception e) {
            throw new T2Error(e.getMessage(),e.getCause()) ;
        } 
        getInternalConfiguration() ;
    }

    /**
     * This will get the engine's internal configuration; we will store various
     * configuration attributes in the variable config, to be reported back
     * to the user.
     */
    private void getInternalConfiguration() {
        config.add("File to read = " + engine.saveFile) ;
        config.add("CUT = " + engine.CUT);
        config.add("Pool = " + engine.pool.getClass().getName()) ;
        if (engine.timeOutPeriod<=0)
            config.add("Time-out = none") ;
        else 
            config.add("Time-out = " + engine.timeOutPeriod) ;
        config.add("Select only violating sequences = " + engine.replayOnlyViolatingTraces) ;
        config.add("Randomly select sequences = " + engine.replayRandomTraces) ;
        config.add("Replay up to this many sequences = " + engine.replayUpToThisMany) ;
        config.add("Stop after this many violations = " + engine.stopAfterThisManyViolations) ;
        config.add("Max. show depth = " + engine.maxShowDepth) ;
        config.add("Print intermediate steps = " + engine.printIntermediateStepsOption) ;
       
    }
    
    public void run()  {
		println("**") ;
		println("** T2 REPLAY tool.") ;
		println("**") ;
        printConfig() ;
        engine.timedRun() ;
        if (reportStream!=null) reportStream.close();
        if (engine.replayresult.numOfViolations>0 
        	|| engine.replayresult.numOfIrrelevantChecks>0
        ) { 	
        	System.out.println("##" + engine.replayresult.firstViolation) ;
        	throw new Violation(engine.replayresult.firstViolation) ;
        }
    }

    public void consoleHelp() {        
        println(Message.GREET);
        println(Message.BEGIN);
        println("Base-engine GENERAL USE:\n");
        println("   java -ea -cp <classpaths> Sequenic.T2.Main [-R] <file-name> <option>*\n");
        println(StringFormater.alignLeft("file-name is the name of the file where the to-be-replayed traces are saved." +
                " Available options are explained below.",
                80, 0));
        println("") ;
        consoleOptionsHelp();
        println(Message.END);
    }

}
