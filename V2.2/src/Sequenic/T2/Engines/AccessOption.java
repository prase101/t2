package Sequenic.T2.Engines;

import java.lang.reflect.*;

/**
 * Used to specify options regarding which fields and methods
 * are included in the testing. The default setting is to include all
 * protected and public non-static fields and methods, including those
 * of superclasses.
 * 
 * <p>Static fields are currently always excluded.
 */
public class AccessOption {
    // public non-static methods are always inlcuded
    Class CUT;
    boolean excludePrivate = true;
    boolean excludeDefault = false;
    boolean excludeProtected = false;
    boolean excludeStatic = false;
    boolean excludeField = false;
    boolean excludeAncestorClassInv = true;
    boolean excludeAncestorFieldsAndMethods = false;

    public AccessOption(Class C) {
        CUT = C;
    }

    public boolean isAllowed(Class declaringClass, int modifier) {

        if (Modifier.isPrivate(modifier) && excludePrivate) {
            return false;
        }
        if (Modifier.isProtected(modifier) && excludeProtected) {
            return false;
        }
        if (isDefault(modifier) && excludeDefault) {
            return false;
        }
        if (Modifier.isStatic(modifier) && excludeStatic) {
            return false;
        }
        if ((declaringClass != CUT) && excludeAncestorFieldsAndMethods) {
            return false;
        }
        return true;
    }

    /**
     * True if the given access modifier represents "default" modifier.
     */
    static public boolean isDefault(int modifier) {
        return !Modifier.isPrivate(modifier) &&
                !Modifier.isProtected(modifier) &&
                !Modifier.isPublic(modifier);
    }
}
	