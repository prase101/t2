/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;
import java.io.* ;

/**
 * T2 uses reporter(s) to produce reports on the results of running a bunch 
 * of tests. The mechanism used needs a bit explanation. 
 *
 * <p> A more stright forward way to report what is going on in a test
 * is by first collecting information as we execute the test. Then we produce 
 * the report at the end of the test. This allows a cleaner implementation of
 * reproting. However such an approach requires us to make deep copies of
 * objects involved in a test. Furthermore we have to keep track of which of those
 * copies actually belong to the same object, and so on. This is complicated!
 *
 * <p> So instead, we choose to 'weave' reporting into test execution. That is,
 * we produce a report on-the-fly. Unfortunately this means that the 
 * entire reporting loop has to be chopped into pieces. Each piece is implemented
 * by a method, which will be called by the test engine at the right point during
 * its execution.
 *
 * <p> This class is however just an abstract class. It simply defines the
 * 'pieces' (as meant above) which every reporter has to implement.  
 * 
 * <p> A simple text-based Reporter is provided. If a new reporter is desired,
 * simply write another subclass of Reporter.
 * 
 * <p> There is also a subclass called ReportersPool, which allows us to bundle
 * multiple reporters to produce multiple reports.
 * 
 * <p> A test engine should actually not call a reporter directly; but instead
 * a reporters pool. This makes it possible to produce multiple reports (e.g.
 * text and HTML).
 * 
 * @see Sequenic.T2.Seq.StdReporter
 * @see Sequenic.T2.Seq.ReprotersPool
 */
abstract public class Reporter {

     // Some variables to control the verbosity of the report:
    
     /**
     * When true (default) will cause parameters passed to a step to be reported.
     */
    public boolean printMethodParamsOption = true ;
    
    /**
     * Default show-depth = 6.
     */
    static final int defaultShowDepth = 6 ;
    
    /**
     * Output stream to send the report to. The default is System.out
     */
    public PrintStream out = System.out ;
    
    public void setOutStream(PrintStream ox) { out = ox ; }
    
    /**
     * To set the maximum depth of objects' interal structures/states that
     * will be reported. Default is 6.
     */ 
    abstract public void setShowDepth(int d) ;
    
    //abstract public void reportTargetObject(Object targetObj) ;
    //abstract public void reportUpdateField(Field field, Object newFieldVal) ;
    //abstract public void reportExecResult(ExecResult res) ;
    abstract public void reportStep(ExecResult res, int stepNr) ;
    //abstract public void reportMethodCall(Method method, Object targetObj, Object receiver, Object[] args) ;
    //abstract public void reportDivergingStep(METARUN step) ;
    
    abstract public void reportTraceBegin() ;
    //abstract public void reportStepNr(int i) ;
    abstract public void reportTraceEnd() ;
    
    abstract public void reportTestSetBegin(Class CUT, String header) ;
    abstract public void reportTestSetEnd(Class CUT, TraceSetExecInfo info, String footer) ;
        
}
