package Sequenic.T2.Seq;

public class TraceExecInfo {
    
    /**
     * The trace to which this exec info belongs to.
     */
    public Trace trace ;
    
    /**
     * The resulting classification after the execution.
     */
    public boolean new_violating_flag = false ;
    public boolean new_diverging_flag = false ;
    
    
    /**
     * The result of the last executed step.
     */
    public ExecResult lastStepResult = null ;
    
    /**
     * If violation is found, at which position. Else -1.
     */
    public int violation_location = -1 ;
    
    public TraceExecInfo(Trace t) { trace = t ; }
    
    /**
     * returns true if a new violation is found.
     */
    public boolean found_new_violation() {
        return new_violating_flag && 
                    (!trace.violating
                    || 
                    (trace.violating && violation_location != trace.trace.size()))
                ;
    }
    
     /**
     * returns true if a violating trace now runs without a violation.
     */
    public boolean violation_disappear() {
        return trace.violating && !new_violating_flag  ;
    }
    
    /**
     * Update the info with the result of a new step. Return false if it is
     * a violating step.
     */
    public boolean update_with_step(ExecResult stepResult, int stepNr) {
        lastStepResult = stepResult ;
        new_violating_flag = stepResult.isAsmOrReqViolating() ;
        new_diverging_flag = stepResult.possiblyDiverging ;
        return !new_violating_flag ;
    }       

}
