/*
 * Copyright 2007 Wishnu Prasetya.
 *
 * This file is part of T2.
 * T2 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T2 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T2.Seq;
import java.io.* ;
import Sequenic.T2.Pool;
import Sequenic.T2.export.Visitor;

/**
 * Representing a MkVal-step in which an object is 'created' by picking it from
 * a {@link Sequenic.T2.Pool object pool}. The Pool-ID of the picked
 * object will be remembered in this step.
 */
public class REF extends MkValStep {

	private static final long serialVersionUID = 1L;
	
    public int index;

    /**
     * @param i The Pool-ID of the object supplied by this step.
     */
    public REF(int i) {
        index = i;
    }

    public Object exec(Class CUT, Pool pool) {
        return pool.get(index);
    }
    
    public String toString() { return "REF " + index ; }
    
    @Override
    public void visit(Visitor visitor) {
		visitor.visit(this);
	}
}
