package Sequenic.T2.export;

/**
 * This class can print the steps in a trace file to XML. 
 * 
 * @author Christiaan Hees
 */
public class XMLPrinter extends TraceExporter {

	public XMLPrinter(String trFileString) {
		super(trFileString);
	}

	@Override
	protected void print() {
		// TODO
	}

	/**
	 * Prints the steps in a trace file as XML.
	 * The first argument should be the name of the trace file.
	 */
	public static void main(String[] args) {
		XMLPrinter xmlp = new XMLPrinter(args[0]);
		xmlp.print();
	}
}
