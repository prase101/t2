package Sequenic.T2.export;

import java.io.PrintStream;

import Sequenic.T2.TrFile;
import Sequenic.T2.Msg.T2Exception;

/**
 * This is the base class for the exporters. You can extend it to export a trace
 * file from T2 to various other representations.
 * 
 * @author Christiaan Hees
 */
public abstract class TraceExporter {
	protected TrFile trFile;
	protected PrintStream out;
	protected Visitor visitor;
	
	/**
	 * Loads the TrFile and sets the default output to System.out.
	 * @param trFileString
	 *     A String representing the name of the TrFile.
	 */
	public TraceExporter(String trFileString) {
		try {
			trFile = TrFile.load(trFileString) ;
		} catch (T2Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		out = System.out;
	}
	
	/**
	 * An implementation of this method should print its data to the
	 * 'out'-PrintStream.
	 */
	protected abstract void print();
}
