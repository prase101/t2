package Sequenic.T2.export;

import Sequenic.T2.Seq.Trace;
import Sequenic.T2.Seq.TraceStep;

/**
 * This class just prints the String value of each TraceStep in the trace file.
 * 
 * @author Christiaan Hees
 */
public class TextPrinter extends TraceExporter {
	private int maxTraceCount = Integer.MAX_VALUE;
	
	public TextPrinter(String trFileString) {
		super(trFileString);
	}
	
	@Override
	protected void print() {
		out.println("CUT: "+trFile.CUTname);
		
		int traceCount = 0;
		for(Trace t : trFile.traces) {
			if(traceCount >= maxTraceCount) break;
			traceCount++;
			
			out.println("---\n"+traceCount);
			out.println(t.creation);
			for(TraceStep ts : t.trace) {
				out.println(ts);
			}
		}
		out.println("test");
	}

	/**
	 * Shows the steps in a trace file.
	 * The first argument should be the name of a trace file.
	 * The optional second argument can be used to limit the number of traces shown.
	 */
	public static void main(String[] args) {
		TextPrinter tp = new TextPrinter(args[0]);
		if(args.length > 1) {
			tp.maxTraceCount = Integer.parseInt(args[1]);
		}
		tp.print();
	}
}
