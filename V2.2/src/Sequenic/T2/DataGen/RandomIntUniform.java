package Sequenic.T2.DataGen;

import java.security.SecureRandom ;

/**
 * This generates a random integer, uniformly distributed between a given
 * [low..high). Note that it is *exclusive* high.
 * 
 * <p>This class uses java's SecureRandom as the random generator. This
 * generator is supposedly non-deterministic.
 */
public class RandomIntUniform extends RandomInt {

    private int low = 0;
    private int high = 0;
    private SecureRandom generator = new SecureRandom();

    public RandomIntUniform(int l, int h) {
    	assert h >= l ;
        low = l ;
        high = h ;
    }

    public Integer nextValue() {
    	return generator.nextInt(high - low) + low ;
    }

    public int getLow() {
        return low;
    }

    public int getHigh() {
        return high;
    }
}
