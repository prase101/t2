package Sequenic.T2.DataGen;

/**
 * An abstract class for randomly generating boolean values. A concrete 
 * implementation may implement a certain distribution.
 */
abstract public class RandomBool {
    
    public double low  ;
    public double high ;
    
    /**
     * Return a random boolean. It works by generating some float x in
     * [0..1) (exclusive 1) according to a certain distribution. A 
     * true is returned iff x in [low..high).
     */
    abstract public boolean nextValue() ;
      
}
