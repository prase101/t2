package Sequenic.T2;
import java.util.* ;
import java.lang.reflect.* ;

/**
 * An predefined implementation of InterfaceMap.
 */
public class InterfaceMap0 extends InterfaceMap {

    HashMap<Class,Class> map ;
    
    
	/**
	 * Creating an empty interface map.
	 */
	public InterfaceMap0() { 
        map = new HashMap<Class,Class>() ; 
    	
		try { map.put(Class.forName("java.lang.Comparable"),
                Class.forName("java.lang.Integer")) ;
		}
		catch (Exception e) { } 
		
		try { map.put(Class.forName("java.lang.Number"),
                Class.forName("java.lang.Integer")) ;
		}
		catch (Exception e) { }

		try { map.put(Class.forName("java.util.Collection"),
                Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.List"),
				Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Queue"),
				Class.forName("java.util.LinkedList")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Set"),
				Class.forName("java.util.HashSet")) ;
		}
		catch (Exception e) { } 

		try { map.put(Class.forName("java.util.Map"),
				Class.forName("java.util.HashMap")) ;
		}
		catch (Exception e) { } 
    
    }

	/**
	 * Obtain an implementaiton of an interface I. Return null if it
	 * can't find I or an implementation of it in the interface map.
	 */
	public Class getImplementation(Class I) { return map.get(I) ; }


}
