package Sequenic.T2.Predicate;

import java.util.*;
import java.lang.reflect.*;

/**
 * Provide some common predicate operators.
 *
 */
public class Operators {
	
	/**
	 * Return true if the predicate p is true on all objects in the given
	 * domain. Else it returns false.
	 */
	static public boolean forall(Collection domain, Predicate p) {
		if (domain==null) return true ;
		for (Object x : domain) {
			if (! p.check(x)) return false ;
		}
		return true ;
	}
	
	/**
	 * Return true if the predicate p is true on some object in the given
	 * domain. Else it returns true.
	 */
	static public boolean exists(Collection domain, Predicate p) {
		if (domain==null) return false ;
		for (Object x : domain) {
			if (p.check(x)) return true ;
		}
		return false ;
	}
	
	/**
	 * M maintains a mapping from the objects in the given domain to
	 * predicates. P must be Predicate or a subclass of it.
	 * 
	 * This returns true, if every object in the domain satisfies its
	 * predicate (as mapped by M). If the object has no mapping yet,
	 * a new instance of P is created, added to M, and we will check if
	 * the object satisfies this new predicate.
	 * 
	 * If the method fails to get the constructor of P, it will either
	 * throw an assert violation, if it is turned on, or return a null.
	 */
	static public Boolean forall(Collection domain, Class P, Map M) {
		
		Predicate p ;
		if (domain==null) return true ;
		
		for (Object x : domain) {
			p = (Predicate) M.get(x) ; 
			if (p == null) { 
				try {
					Constructor C = P.getConstructor((Class []) null) ; 
					p = (Predicate) C.newInstance((Object []) null) ;
					M.put(x,p) ;
				}
				catch (Exception e) {
					assert false ;
					return null ;
				}
			}
			if (!p.check(x)) return false ;		
		}
		return true ;
	}
	
	
}
