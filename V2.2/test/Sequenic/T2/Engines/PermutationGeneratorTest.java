package Sequenic.T2.Engines;

import java.math.BigInteger;
import Sequenic.T2.Obj.Show ;
import org.junit.Test;

/**
 *
 * @author underdarkprime
 */
public class PermutationGeneratorTest {

    static void printComb(int[] a) {
        System.out.print(">>> ") ;
        for (int i=0; i<a.length; i++) System.out.print("," + a[i]) ;
        System.out.println("") ;
    }
    
    
    @Test
    public void test0() {
    	int n = 3 ;
    	PermutationGenerator gen = new PermutationGenerator(n) ;
        int[] r = new int[n] ;
        int N =  gen.getTotal() ;
        System.out.println(">>> number of combinations: " + N ) ;
        int i = 1 ;
        while (gen.hasMore()) {
        	gen.getNext(r) ;
        	System.out.println(">>> [" + i + "] " + Show.show(r)) ;
        	i++ ;
        }
    }

}