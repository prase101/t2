package Sequenic.T2.Engines;

import java.math.BigInteger;
import Sequenic.T2.Obj.Show ;
import org.junit.Test;

public class SequenceGeneratorTest {

    static void printComb(int[] a) {
        System.out.print(">>> ") ;
        for (int i=0; i<a.length; i++) System.out.print("," + a[i]) ;
        System.out.println("") ;
    }
    
    
    @Test
    public void test0() {
    	int n = 5 ;
    	int r = 3 ;
    	SequenceGenerator gen = new SequenceGenerator(n,r) ;
        int[] res = new int[r] ;
        long N =  gen.getTotal() ;
        System.out.println(">>> number of combinations: " + N ) ;
        int i = 1 ;
        while (gen.hasMore()) {
        	gen.getNext(res) ;
        	System.out.println(">>> [" + i + "] " + Show.show(res)) ;
        	i++ ;
        }
    }

}