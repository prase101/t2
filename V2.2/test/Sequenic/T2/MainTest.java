package Sequenic.T2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author underdarkprime
 */
public class MainTest {

    public MainTest() {
    }

    @Test
    public void testMainHelp() {
        System.out.println("@@@ Testing Main's help...");
        Main tool = new Main();
        tool.main("--help") ;
    }
    
    @Test
    public void testBaseEngineHelp() {
        System.out.println("@@@ Testing Base Engine's help...");
        Main tool = new Main();
        tool.main("-E ") ;
    }
    
    @Test
    public void testReplayEngineHelp() {
        System.out.println("@@@ Testing Replay Engine's help...");
        Main tool = new Main();
        tool.main("-R ") ;
    }
    
    //@Test
    public void testInvalidOption() {
        System.out.println("@@@ Testing invalid option...");
        Main tool = new Main();
        tool.main(C1.class.getName() + " --invalidoption ") ;
    }
    
    static public class C1 {
        int x ;
        int m(int y) { x=x+y ; return x ; }
        void ouch() { assert false; }
    }
    
    //@Test
    public void testBaseEngine1() {
        System.out.println("@@@ Testing plugin to Base Engine...");
        Main tool = new Main();
        tool.main("-E Sequenic.T2.MainTest$C1") ;
    }
    
    
}