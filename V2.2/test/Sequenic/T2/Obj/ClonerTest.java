package Sequenic.T2.Obj;

import Sequenic.T2.*;
import java.io.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClonerTest {

    public static class Graph2 implements Serializable {

        int val;
        Graph2 link1 = null;
        Graph2 link2 = null;

        public Graph2(int x) { val = x ; }

        public Graph2(int x, Graph2 a, Graph2 b) {
            val = x;
            link1 = a; link2 = b;
        }
    }
    
    public static class  Dummy { }

    @Test
    public void test1() {
        System.out.println("@@@ Using T2 to auto-test Cloner ...");
        Class C = Cloner.class;
        TrFile.delete(C);
        Main.Junit(C.getName() + " --debug -n 20000 --elemty=" + Graph2.class.getName());
        //Debug.assertTrfEmpty(C);
    
        TrFile.delete(C);
        Main.Junit(C.getName() + " --debug -n 20000 --elemty=" + Dummy.class.getName());
        //Debug.assertTrfEmpty(C);
    }
}